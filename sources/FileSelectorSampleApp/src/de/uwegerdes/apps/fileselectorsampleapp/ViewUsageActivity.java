package de.uwegerdes.apps.fileselectorsampleapp;

import java.io.File;

import de.uwegerdes.apps.fileselectorlib.FileSelector;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * sample usage for FileSelectorView as widget in res/layout/view_activity.xml
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class ViewUsageActivity extends Activity {
	public final static String TAG = "ViewUsageActivity";

	private Intent result;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.view_activity);

		// finish button to show separated file selector view action
		// from activity action
		Button finishButton = (Button) findViewById(R.id.view_usage_finish);
		finishButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	// see res/layout/view_activity.xml / app:onOkClick="callbackMethod"
	// parameter is selected file on ok click or null if cancel clicked
	public void callbackMethod(File file) {
		Toast.makeText(this, "ok button clicked: " + file.getAbsolutePath(),
				Toast.LENGTH_LONG).show();
		result = new Intent();
		result.putExtra(FileSelector.SELECTED_FILE, file);
		setResult(RESULT_OK, result);
		// finish();
	}

	// see res/layout/view_activity.xml / app:onCancelClick="callbackMethod"
	// please note: the methods _may_ have the same name but have different
	// paramters
	public void callbackMethod() {
		Toast.makeText(this, "cancel button clicked", Toast.LENGTH_SHORT)
				.show();
		result = new Intent();
		setResult(RESULT_CANCELED);
		// finish();
	}
}
