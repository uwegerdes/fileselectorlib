package de.uwegerdes.apps.fileselectorsampleapp;

import java.io.File;

import de.uwegerdes.apps.fileselectorlib.app.FileSaveAsDialogFragment;
import de.uwegerdes.apps.fileselectorlib.app.FileSelectorDialogFragment;
import de.uwegerdes.apps.fileselectorlib.app.FileSelectorFragmentActivity;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView;
import de.uwegerdes.apps.fileselectorlib.widget.FileSaveAsView;
import de.uwegerdes.apps.fileselectorlib.FileSelector;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Sample usage for FileSelectorLib and project for tests
 * 
 * @author uwe
 */
public class MainActivity extends FragmentActivity implements
		FileSelectorView.OnFileSelectedListener {
	public final static String TAG = "MainActivity";

	private final static int ACTIVITY_REQUEST_CODE = 1;

	private TextView buttonSelectDirectory;
	private TextView buttonSelectFile;
	private TextView buttonSelectFileExtension;
	private TextView buttonSelectFileMimeType;
	private TextView buttonSelectFileBaseDirectory;
	private TextView buttonSelectDialog;
	private TextView buttonSelectViewUsage;
	private TextView buttonSelectFileSaveAsDialog;

	private FileSelectorDialogFragment fileSelectorDialogFragment;
	private FileSaveAsDialogFragment fileSaveAsDialogFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		buttonSelectDirectory = (TextView) findViewById(R.id.buttonSelectDirectory);
		buttonSelectFile = (TextView) findViewById(R.id.buttonSelectFile);
		buttonSelectFileExtension = (TextView) findViewById(R.id.buttonSelectFileExtension);
		buttonSelectFileMimeType = (TextView) findViewById(R.id.buttonSelectFileMimeType);
		buttonSelectFileBaseDirectory = (TextView) findViewById(R.id.buttonSelectFileBaseDirectory);
		buttonSelectDialog = (TextView) findViewById(R.id.buttonSelectDialog);
		buttonSelectViewUsage = (TextView) findViewById(R.id.buttonSelectViewUsage);
		buttonSelectFileSaveAsDialog = (TextView) findViewById(R.id.buttonSelectFileSaveAsDialog);

		buttonSelectDirectory.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				selectDirectory();
			}
		});

		buttonSelectFile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				selectFile();
			}
		});

		buttonSelectFileExtension.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				selectFileExtension("txt xml json");
			}
		});

		buttonSelectFileMimeType.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				selectFileMimeType("text/plain text/html text/xml");
			}
		});

		buttonSelectFileBaseDirectory.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String baseDir = Environment.getExternalStorageDirectory()
						.getPath();
				baseDir += "/" + getString(R.string.app_name);
				selectFileBaseDirectory(baseDir);
			}
		});

		buttonSelectDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				selectDialog();
			}
		});

		buttonSelectViewUsage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				selectViewUsage();
			}
		});

		buttonSelectFileSaveAsDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				selectFileSaveAsDialog();
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		if (fileSaveAsDialogFragment != null) {
			setFileSelectDialogListener();
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ACTIVITY_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				File file = (File) data
						.getSerializableExtra(FileSelector.SELECTED_FILE);
				msg("selected: " + file.getAbsolutePath());
			} else if (resultCode == Activity.RESULT_CANCELED) {
				msg("canceled");
			}
		} else {
			Log.e(TAG, "unknown requestCode");
		}
	}

	/**
	 * start selection of a directory
	 */
	private void selectDirectory() {
		Intent intent = new Intent(this, FileSelectorFragmentActivity.class);
		intent.putExtra(FileSelector.SELECT_TYPE,
				FileSelector.SELECT_TYPE_DIRECTORY);
		startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
	}

	/**
	 * start selection of a file
	 */
	private void selectFile() {
		Intent intent = new Intent(this, FileSelectorFragmentActivity.class);
		intent.putExtra(FileSelector.SELECT_TYPE, FileSelector.SELECT_TYPE_FILE);
		startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
	}

	/**
	 * start selection of a file for a list of extensions
	 */
	private void selectFileExtension(String extensions) {
		Intent intent = new Intent(this, FileSelectorFragmentActivity.class);
		intent.putExtra(FileSelector.SELECT_TYPE, FileSelector.SELECT_TYPE_FILE);
		intent.putExtra(FileSelector.EXTENSIONS, extensions);
		startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
	}

	/**
	 * start selection of a file for a list of mime types
	 */
	private void selectFileMimeType(String mimeTypes) {
		Intent intent = new Intent(this, FileSelectorFragmentActivity.class);
		intent.putExtra(FileSelector.SELECT_TYPE, FileSelector.SELECT_TYPE_FILE);
		intent.putExtra(FileSelector.MIMETYPES, mimeTypes);
		startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
	}

	/**
	 * start selection of a file for a list of mime types
	 */
	private void selectFileBaseDirectory(String directory) {
		Intent intent = new Intent(this, FileSelectorFragmentActivity.class);
		intent.putExtra(FileSelector.SELECT_TYPE, FileSelector.SELECT_TYPE_FILE);
		intent.putExtra(FileSelector.BASE_DIRECTORY, directory);
		startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
	}

	/**
	 * start selection as a dialog
	 */
	private void selectDialog() {
		if (fileSelectorDialogFragment == null) {
			fileSelectorDialogFragment = new FileSelectorDialogFragment();
			// listener at the moment overridden by implementation of interface
			// dialogFragment.setOnFileSelectedListener(fileSelectedListener);
			Bundle args = new Bundle();
			args.putString(FileSelector.SELECT_TYPE,
					FileSelector.SELECT_TYPE_FILE);
			args.putString(FileSelector.TITLE, "File selection");
			fileSelectorDialogFragment.setArguments(args);
		} else {
			Log.d(TAG, "dialog fragment reused");
		}
		fileSelectorDialogFragment.show(getSupportFragmentManager(), TAG);
	}

	// listener deactivated - not used in selectDialog()
	// /**
	// * Sample file selected listener - not used at the moment
	// */
	// private FileSelectorView.OnFileSelectedListener fileSelectedListener =
	// new FileSelectorView.OnFileSelectedListener() {
	// @Override
	// public void onFileSelected(File file) {
	// if (file != null) {
	// msg(file.getAbsolutePath());
	// }
	// }
	// @Override
	// public void onCancel() {
	// msg("cancel clicked");
	// }
	// };

	/**
	 * start selection of a file for a list of mime types
	 */
	private void selectViewUsage() {
		Intent intent = new Intent(this, ViewUsageActivity.class);
		startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
	}

	/**
	 * start selection as a dialog
	 */
	private void selectFileSaveAsDialog() {
		if (fileSaveAsDialogFragment == null) {
			fileSaveAsDialogFragment = new FileSaveAsDialogFragment();
			Bundle args = new Bundle();
			args.putString(FileSelector.TITLE, "File save as");
			args.putString(FileSelector.FILENAME, "sample_file");
			args.putString(FileSelector.FILETYPE, "txt");
			File currentDirectory = new File(
					Environment.getExternalStorageDirectory(),
					getString(R.string.app_name));
			args.putString(FileSelector.CURRENT_DIRECTORY,
					currentDirectory.getAbsolutePath());
			fileSaveAsDialogFragment.setArguments(args);
		} else {
			Log.d(TAG, "dialog fragment reused");
		}
		setFileSelectDialogListener();
		fileSaveAsDialogFragment.show(getSupportFragmentManager(), TAG);
	}

	private void setFileSelectDialogListener() {
		fileSaveAsDialogFragment
		.setOnFileSelectedListener(new FileSaveAsView.OnFileSelectedListener() {
			@Override
			public void onFileSelected(File file) {
				if (file != null) {
					msg("save file: " + file.getAbsolutePath());
				}
			}

			@Override
			public void onCancel() {
				msg("cancel clicked");
			}
		});

	}
	/**
	 * Sample file save as listener - not used at the moment
	 */
	private FileSaveAsView.OnFileSelectedListener fileSaveAsListener = new FileSaveAsView.OnFileSelectedListener() {
		@Override
		public void onFileSelected(File file) {
			if (file != null) {
				msg("save file: " + file.getAbsolutePath());
			}
		}

		@Override
		public void onCancel() {
			msg("cancel clicked");
		}
	};

	private void msg(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}

	// for tests
	public FileSelectorDialogFragment getFileSelectorDialogFragment() {
		return fileSelectorDialogFragment;
	}

	// for tests
	public FileSaveAsDialogFragment getFileSaveAsDialogFragment() {
		return fileSaveAsDialogFragment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.
	 * OnFileSelectedListener#onFileSelected(java.io.File)
	 */
	@Override
	public void onFileSelected(File file) {
		Log.e(TAG, "main received: " + file.getAbsolutePath());
		Toast.makeText(this, "main received: " + file.getAbsolutePath(),
				Toast.LENGTH_LONG).show();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.
	 * OnFileSelectedListener#onCancel()
	 */
	@Override
	public void onCancel() {
		Log.e(TAG, "main received cancelled");
		Toast.makeText(this, "main received canceled", Toast.LENGTH_LONG)
				.show();
	}
}
