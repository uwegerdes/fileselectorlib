/********************************************************************
 * 
 * Read directory content for a given directory. Select files matching
 * extensions or mimeTypes. If select mode is directory than no files are
 * listed.
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 * 
 ********************************************************************/
package de.uwegerdes.apps.fileselectorlib.io;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import de.uwegerdes.apps.fileselectorlib.FileSelector;

import android.webkit.MimeTypeMap;

public class DirectoryReader {
	public final static String TAG = "DirectoryReader";

	private String selectType = "";
	private String extensions;

	/**
	 * Create a new DirectoryReader, set the select mode (file or directory) and
	 * set up the list of extensions.
	 * 
	 * MimeTypes are translated back to file extensions by MimeTypeMap - blame
	 * it for poor results
	 * 
	 * @param selectType
	 *            must be FileSelector.TYPE_FILE or FileSelector.TYPE_DIRECTORY.
	 * @param extensions
	 *            a list of file extensions like "jpg png gif"
	 * @param mimeTypes
	 *            a list of mime types (no grouping /*) like
	 *            "text/plain text/xml"
	 */
	public DirectoryReader(String selectType, String extensions,
			String mimeTypes) {
		this.extensions = extensions;
		this.selectType = selectType;
		if (mimeTypes != null && mimeTypes.length() > 0) {
			MimeTypeMap map = MimeTypeMap.getSingleton();
			for (String mime : mimeTypes.split(" ")) {
				String ext = map.getExtensionFromMimeType(mime);
				if (ext != null) {
					this.extensions += " " + ext;
				}
			}
		}
	}

	/**
	 * Read the content of a directory
	 * 
	 * @param currentDirectory
	 *            file pointing to the directory
	 * @return list of files found there and matching the conditions
	 */
	public ArrayList<File> readDirectory(File dir) {
		ArrayList<File> files = new ArrayList<File>();
		if (dir.exists() && dir.isDirectory()) {
			// collect directories
			ArrayList<File> dirs = new ArrayList<File>();
			dirs.addAll(Arrays.asList(dir.listFiles(new FileFilter() {
				public boolean accept(File pathname) {
					if (pathname.getPath().contains("/.")) {
						return false;
					}
					return pathname.isDirectory();
				}
			})));
			for (File path : dirs) {
				files.add(path);
			}
			// collect files
			if (selectType != null && selectType.equals(FileSelector.SELECT_TYPE_FILE)) {
				File[] paths = dir.listFiles(new GenericFileFilter(extensions));
				for (File path : paths) {
					files.add(path);
				}
			}
			// sort the list
			Collections.sort(files, new Comparator<File>() {
				public int compare(File file1, File file2) {
					return file1.getName().compareToIgnoreCase(file2.getName());
				}
			});
		}
		return files;
	}

	/**
	 * file filter by extension
	 */
	private static class GenericFileFilter implements FilenameFilter {
		private final TreeSet<String> exts = new TreeSet<String>();

		public GenericFileFilter(String extensions) {
			if (extensions == null) {
				return;
			}
			// build up extensions list
			Iterator<String> extList = Arrays.asList(extensions.split(" "))
					.iterator();
			while (extList.hasNext()) {
				exts.add("." + extList.next().toLowerCase().trim());
			}
			exts.remove("");
			exts.remove(".");
		}

		public boolean accept(File dir, String name) {
			if (name.startsWith(".")) {
				return false;
			}
			File file = new File(dir, name);
			if (file.isDirectory())
				return false; // no directories here
			if (exts.size() == 0)
				return true; // take file if no extensions list
			for (String ext : exts) {
				if (name.toLowerCase().endsWith(ext)) {
					return true; // take file if extension matches
				}
			}
			return false; // file does not match extensions list
		}
	}

}
