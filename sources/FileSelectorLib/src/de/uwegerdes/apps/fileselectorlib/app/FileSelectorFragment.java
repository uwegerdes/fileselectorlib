/**
 * 
 */
package de.uwegerdes.apps.fileselectorlib.app;

import java.io.File;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class FileSelectorFragment extends Fragment {
	public final static String TAG = "FileSelectorFragment";

	private String selectType = "";
	private String basedir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String extensions = "";
	private String mimeTypes = "";
	private String selectedFile = null;
	private String currentDirectory = null;

	private Bundle arguments;
	private FileSelectorView fileSelectorView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		arguments = getArguments();
		if (savedInstanceState != null) {
			arguments = savedInstanceState;
		}
		if (arguments != null) {
			if (arguments.containsKey(FileSelector.SELECT_TYPE)) {
				this.selectType = arguments.getString(FileSelector.SELECT_TYPE);
			}
			if (arguments.containsKey(FileSelector.BASE_DIRECTORY)) {
				String basedir = arguments
						.getString(FileSelector.BASE_DIRECTORY);
				if (basedir.startsWith("/")) {
					this.basedir = basedir;
				} else {
					Log.e(TAG,
							"error: directory name must start with / but is: "
									+ basedir);
				}
			}
			if (arguments.containsKey(FileSelector.EXTENSIONS)) {
				this.extensions = arguments.getString(FileSelector.EXTENSIONS);
			}
			if (arguments.containsKey(FileSelector.MIMETYPES)) {
				this.mimeTypes = arguments.getString(FileSelector.MIMETYPES);
			}
			if (arguments.containsKey(FileSelector.CURRENT_DIRECTORY)) {
				this.currentDirectory = arguments
						.getString(FileSelector.CURRENT_DIRECTORY);
			}
			if (arguments.containsKey(FileSelector.SELECTED_FILE)) {
				this.selectedFile = arguments
						.getString(FileSelector.SELECTED_FILE);
			}
		}
		if (fileSelectorView == null) {
			fileSelectorView = new FileSelectorView(getActivity(), selectType,
					extensions, mimeTypes, basedir, currentDirectory,
					selectedFile);
		} else {
			Log.e(TAG, "View reused???");
		}
		return fileSelectorView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		arguments.putString(FileSelector.CURRENT_DIRECTORY,
				fileSelectorView.getCurrentDirectory().getAbsolutePath());
		File selectedFile = fileSelectorView.getSelectedFile();
		if (selectedFile != null) {
			arguments.putString(FileSelector.SELECTED_FILE,
					selectedFile.getAbsolutePath());
		} else {
			arguments.putString(FileSelector.SELECTED_FILE, null);
		}
		outState.putAll(arguments);
		super.onSaveInstanceState(outState);
	}
}
