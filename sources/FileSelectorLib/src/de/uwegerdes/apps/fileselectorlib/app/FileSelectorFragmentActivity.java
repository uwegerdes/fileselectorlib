/**
 * 
 */
package de.uwegerdes.apps.fileselectorlib.app;

import java.io.File;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView;
import de.uwegerdes.apps.fileselectorlib.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class FileSelectorFragmentActivity extends FragmentActivity implements
		FileSelectorView.OnFileSelectedListener {
	public final static String TAG = "FileSelectorFragmentActivity";

	private FileSelectorFragment fileSelectorFragment;
	private Bundle arguments;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fileselector_fragment_activity);
		if (savedInstanceState != null) {
			arguments = savedInstanceState;
		} else {
			arguments = getIntent().getExtras();
		}
		FragmentManager fm = getSupportFragmentManager();
		if (fm.findFragmentByTag(FileSelectorFragment.TAG) != null) {
			fileSelectorFragment = (FileSelectorFragment) fm
					.findFragmentByTag(FileSelectorFragment.TAG);
		} else {
			fileSelectorFragment = new FileSelectorFragment();
			fileSelectorFragment.setArguments(arguments);
			FragmentTransaction ft = fm.beginTransaction();
			ft.add(R.id.fileselector_fragment_Container, fileSelectorFragment,
					FileSelectorFragment.TAG);
			ft.commit();
		}
	}

	@Override
	public void onFileSelected(File file) {
		Intent result = new Intent();
		result.putExtra(FileSelector.SELECTED_FILE, file);
		setResult(RESULT_OK, result);
		finish();
	}

	@Override
	public void onCancel() {
		setResult(RESULT_CANCELED);
		finish();
	}
}
