/**
 * 
 */
package de.uwegerdes.apps.fileselectorlib.app;

import java.io.File;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.OnFileSelectedListener;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.OnSelectionChangeListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class FileSelectorDialogFragment extends DialogFragment {
	public final static String TAG = "FileSelectorDialogFragment";

	private FileSelectorView fileselectorView;
	private Bundle arguments;
	private FileSelectorView.OnFileSelectedListener onFileSelectedlistener;

	private String title;
	private String selectType = FileSelector.SELECT_TYPE_FILE;
	private String basedir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String extensions = "";
	private String mimeTypes = "";
	private String currentDirectory = null;
	private String selectedFile = null;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		arguments = getArguments();
		if (savedInstanceState != null) {
			arguments = savedInstanceState;
		}
		if (arguments != null) {
			if (arguments.containsKey(FileSelector.TITLE)) {
				this.title = arguments.getString(FileSelector.TITLE);
			}
			if (arguments.containsKey(FileSelector.SELECT_TYPE)) {
				this.selectType = arguments.getString(FileSelector.SELECT_TYPE);
			}
			if (arguments.containsKey(FileSelector.BASE_DIRECTORY)) {
				String basedir = arguments
						.getString(FileSelector.BASE_DIRECTORY);
				if (basedir.startsWith("/")) {
					this.basedir = basedir;
				} else {
					Log.e(TAG,
							"error: directory name must start with / but is: "
									+ basedir);
				}
			}
			if (arguments.containsKey(FileSelector.EXTENSIONS)) {
				this.extensions = arguments.getString(FileSelector.EXTENSIONS);
			}
			if (arguments.containsKey(FileSelector.MIMETYPES)) {
				this.mimeTypes = arguments.getString(FileSelector.MIMETYPES);
			}
			if (arguments.containsKey(FileSelector.CURRENT_DIRECTORY)) {
				this.currentDirectory = arguments
						.getString(FileSelector.CURRENT_DIRECTORY);
			}
			if (arguments.containsKey(FileSelector.SELECTED_FILE)) {
				this.selectedFile = arguments
						.getString(FileSelector.SELECTED_FILE);
			}
		}

		builder.setTitle(title);
		fileselectorView = new FileSelectorView(getActivity(), selectType,
				extensions, mimeTypes, basedir, currentDirectory, selectedFile);
		fileselectorView.hideButtons();
		builder.setView(fileselectorView);
		if (onFileSelectedlistener != null) {
			// listener set before dialog.show();
			fileselectorView.setOnFileSelectedListener(onFileSelectedlistener);
			fileselectorView
					.setOnSelectionChangeListener(onSelectionChangeListener);
		}
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						File selectedFile = fileselectorView.getSelectedFile();
						if (onFileSelectedlistener != null) {
							onFileSelectedlistener.onFileSelected(selectedFile);
						} else if (getActivity() instanceof OnFileSelectedListener) {
							((OnFileSelectedListener) getActivity())
									.onFileSelected(selectedFile);
						} else {
							message("Aktion kann nicht durchgeführt werden");
						}
					}
				});
		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						if (onFileSelectedlistener != null) {
							onFileSelectedlistener.onCancel();
						} else if (getActivity() instanceof OnFileSelectedListener) {
							((OnFileSelectedListener) getActivity()).onCancel();
						} else {
							message("Aktion kann nicht durchgeführt werden");
						}
					}
				});
		builder.setInverseBackgroundForced(true);
		return builder.create();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnFileSelectedListener) {
			onFileSelectedlistener = (OnFileSelectedListener) activity;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		setOkButtonEnabled(selectedFile != null);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		arguments.putString(FileSelector.CURRENT_DIRECTORY, currentDirectory);
		arguments.putString(FileSelector.SELECTED_FILE, selectedFile);
		outState.putAll(arguments);
		super.onSaveInstanceState(outState);
	}

	/**
	 * set the listener for file selection ok / cancel
	 * 
	 * @param onFileSelectedlistener
	 */
	public void setOnFileSelectedListener(
			FileSelectorView.OnFileSelectedListener onFileSelectedlistener) {
		this.onFileSelectedlistener = onFileSelectedlistener;
		if (fileselectorView != null) {
			fileselectorView.setOnFileSelectedListener(onFileSelectedlistener);
		}
	}

	/**
	 * listener for selection state change - used to set ok button state
	 */
	private FileSelectorView.OnSelectionChangeListener onSelectionChangeListener = new OnSelectionChangeListener() {
		@Override
		public void onSelectionChanged(File file) {
			setOkButtonEnabled(file != null);
			if (file != null) {
				selectedFile = file.getAbsolutePath();
			} else {
				selectedFile = null;
			}
		}

		@Override
		public void onDirectoryChanged(File directory) {
			currentDirectory = directory.getAbsolutePath();
		}
	};

	/**
	 * set button ok enabled if a file is selected
	 * 
	 * @param enabled
	 *            state to set
	 */
	private void setOkButtonEnabled(boolean enabled) {
		((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE)
				.setEnabled(enabled);
	}

	/**
	 * get the alert dialog object (with buttons ok / cancel) - used for tests
	 * 
	 * @return get the actual dialog as AlertDialog
	 */
	public AlertDialog getAlertDialog() {
		return (AlertDialog) getDialog();
	}

	public void message(String message) {
		Log.e(TAG, message);
		Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
	}
}
