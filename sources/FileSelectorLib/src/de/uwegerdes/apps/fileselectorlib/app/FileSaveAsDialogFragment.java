/**
 * 
 */
package de.uwegerdes.apps.fileselectorlib.app;

import java.io.File;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.widget.FileSaveAsView;
import de.uwegerdes.apps.fileselectorlib.widget.FileSaveAsView.OnFileSelectedListener;
import de.uwegerdes.apps.fileselectorlib.widget.FileSaveAsView.OnSelectionChangeListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

/**
 * Dialog for file save as - supply currentDir, filename and filetype
 * 
 * returns a File object with the selection or null if canceled
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class FileSaveAsDialogFragment extends DialogFragment {
	public final static String TAG = "FileSaveAsDialogFragment";

	private FileSaveAsView fileSaveAsView;
	private Bundle arguments;
	private FileSaveAsView.OnFileSelectedListener onFileSelectedlistener;

	private String title;
	private String basedir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String filename = "";
	private String filetype = "";
	private String currentDirectory = null;
	private String selectedFile = null;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		arguments = getArguments();
		if (savedInstanceState != null) {
			arguments = savedInstanceState;
		}
		if (arguments != null) {
			if (arguments.containsKey(FileSelector.TITLE)) {
				this.title = arguments.getString(FileSelector.TITLE);
			}
			if (arguments.containsKey(FileSelector.BASE_DIRECTORY)) {
				String basedir = arguments
						.getString(FileSelector.BASE_DIRECTORY);
				if (basedir.startsWith("/")) {
					this.basedir = basedir;
				} else {
					Log.e(TAG,
							"error: directory name must start with / but is: "
									+ basedir);
				}
			}
			if (arguments.containsKey(FileSelector.FILENAME)) {
				this.filename = arguments.getString(FileSelector.FILENAME);
			}
			if (arguments.containsKey(FileSelector.FILETYPE)) {
				this.filetype = arguments.getString(FileSelector.FILETYPE);
			}
			if (arguments.containsKey(FileSelector.CURRENT_DIRECTORY)) {
				this.currentDirectory = arguments
						.getString(FileSelector.CURRENT_DIRECTORY);
			} else {
				this.currentDirectory = this.basedir;
			}
			if (arguments.containsKey(FileSelector.SELECTED_FILE)) {
				this.selectedFile = arguments
						.getString(FileSelector.SELECTED_FILE);
			} else {
				if (currentDirectory != null && currentDirectory.length() > 0
						&& filename != null && filename.length() > 0
						&& filetype != null && filetype.length() > 0) {
					this.selectedFile = this.currentDirectory + File.separator
							+ this.filename + "." + this.filetype;
				}
			}
		}

		builder.setTitle(title);
		fileSaveAsView = new FileSaveAsView(getActivity(), filename, filetype,
				basedir, currentDirectory);
		fileSaveAsView.hideButtons();
		builder.setView(fileSaveAsView);
		if (onFileSelectedlistener != null) {
			// listener set before dialog.show();
			Log.e(TAG, "listener bereits vorhanden in onCreateDialog");
			fileSaveAsView.setOnFileSelectedListener(onFileSelectedlistener);
			fileSaveAsView
					.setOnSelectionChangeListener(onSelectionChangeListener);
		}
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						File selectedFile = fileSaveAsView.getSelectedFile();
						if (onFileSelectedlistener != null) {
							onFileSelectedlistener.onFileSelected(selectedFile);
						} else if (getActivity() instanceof OnFileSelectedListener) {
							((OnFileSelectedListener) getActivity())
									.onFileSelected(selectedFile);
						} else {
							message("Aktion kann nicht durchgeführt werden");
						}
					}
				});
		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						if (onFileSelectedlistener != null) {
							onFileSelectedlistener.onCancel();
						} else if (getActivity() instanceof OnFileSelectedListener) {
							((OnFileSelectedListener) getActivity()).onCancel();
						} else {
							message("Aktion kann nicht durchgeführt werden");
						}
					}
				});
		builder.setInverseBackgroundForced(true);
		return builder.create();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnFileSelectedListener) {
			Log.e(TAG, "autolistener in onAttach");
			onFileSelectedlistener = (OnFileSelectedListener) activity;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		setOkButtonState();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		arguments.putString(FileSelector.CURRENT_DIRECTORY, currentDirectory);
		arguments.putString(FileSelector.SELECTED_FILE, selectedFile);
		outState.putAll(arguments);
		super.onSaveInstanceState(outState);
	}

	/**
	 * set the listener for file selection ok / cancel
	 * 
	 * @param onFileSelectedlistener
	 */
	public void setOnFileSelectedListener(
			FileSaveAsView.OnFileSelectedListener onFileSelectedlistener) {
		Log.e(TAG, "listener gesetzt");
		this.onFileSelectedlistener = onFileSelectedlistener;
		if (fileSaveAsView != null) {
			fileSaveAsView.setOnFileSelectedListener(onFileSelectedlistener);
		}
	}

	/**
	 * listener for selection state change - used to set ok button state
	 */
	private FileSaveAsView.OnSelectionChangeListener onSelectionChangeListener = new OnSelectionChangeListener() {
		@Override
		public void onSelectionChanged(File file) {
			if (file != null) {
				selectedFile = file.getAbsolutePath();
			} else {
				selectedFile = null;
			}
			setOkButtonState();
		}

		@Override
		public void onDirectoryChanged(File directory) {
			currentDirectory = directory.getAbsolutePath();
		}
	};

	/**
	 * set button ok enabled if a file is selected
	 * 
	 * @param enabled
	 *            state to set
	 */
	private void setOkButtonState() {
		boolean okButtonEnabled = (selectedFile != null && fileSaveAsView
				.okEnabled());
		((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE)
				.setEnabled(okButtonEnabled);
	}

	/**
	 * get the alert dialog object (with buttons ok / cancel) - used for tests
	 * 
	 * @return get the actual dialog as AlertDialog
	 */
	public AlertDialog getAlertDialog() {
		return (AlertDialog) getDialog();
	}

	public void message(String message) {
		Log.e(TAG, message);
		Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
	}

}
