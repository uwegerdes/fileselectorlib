package de.uwegerdes.apps.fileselectorlib.widget;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.R;
import de.uwegerdes.apps.fileselectorlib.io.DirectoryReader;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Widget for file selection for given directory, extension or mime type list
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class FileSaveAsView extends FrameLayout {
	public final static String TAG = "FileSaveAsView";

	private Context context;
	private LayoutInflater mInflater;
	private ViewGroup parent;

	private String filename;
	private String filetype;
	private File baseDirectory;
	private File currentDirectory;
	private File selectedFilex = null;
	private String onOkClickCallback;
	private String onCancelClickCallback;

	private EditText file_save_as_Filename;
	private TextView file_save_as_Filetype;
	private TextView file_save_as_Directory;
	private ListView file_save_as_ListView;
	private FileSelectorAdapter file_save_as_ListAdapter;
	private CheckBox file_save_as_Overwrite;
	private ImageButton file_save_as_ButtonUp;
	private Button file_save_as_ButtonOk;
	private Button file_save_as_ButtonCancel;
	private OnFileSelectedListener onFileSelectedListener;
	private OnSelectionChangeListener onSelectionChangeListener;

	private View selectedRadio = null;

	/**
	 * @param context
	 */
	public FileSaveAsView(Context context) {
		super(context);
		this.context = getContext();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public FileSaveAsView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = getContext();
		obtainAttributes(attrs);
		initView();
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public FileSaveAsView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = getContext();
		obtainAttributes(attrs);
		initView();
	}

	/**
	 * @param context
	 */
	public FileSaveAsView(Context context, String filename, String filetype,
			String baseDirectory) {
		super(context);
		this.context = getContext();
		this.filename = filename;
		this.filetype = filetype;
		this.baseDirectory = new File(baseDirectory);
		this.currentDirectory = new File(baseDirectory);
		initView();
	}

	/**
	 * TODO: comment
	 * 
	 * @param context
	 * @param selectedFile
	 */
	public FileSaveAsView(Context context, String filename, String filetype,
			String baseDirectory, String currentDirectory) {
		super(context);
		this.context = getContext();
		this.filename = filename;
		this.filetype = filetype;
		this.baseDirectory = new File(baseDirectory);
		if (currentDirectory != null) {
			this.currentDirectory = new File(currentDirectory);
		} else {
			this.currentDirectory = new File(baseDirectory);
		}
		initView();
	}

	// Save state ///////////////////////////////////////////////////
	public class SavedState extends BaseSavedState {
		String currentDirectoryString;
		String selectedFileString;

		public SavedState(Parcelable superState) {
			super(superState);
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeString(currentDirectoryString);
			out.writeString(selectedFileString);
		}

		@Override
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append("FileSelectorView.SavedState{");
			buffer.append(Integer.toHexString(System.identityHashCode(this)));
			buffer.append(" currentDirectory=" + currentDirectoryString);
			buffer.append(" selectedFile=" + selectedFileString);
			buffer.append("}");
			return buffer.toString();
		}

		public final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};

		private SavedState(Parcel in) {
			super(in);
			currentDirectoryString = in.readString();
			selectedFileString = in.readString();
		}
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		File selectedFile = getSelectedFile();
		if (currentDirectory != baseDirectory || selectedFile != null) {
			SavedState ss = new SavedState(superState);
			// TODO: Should also save the current scroll position!
			ss.currentDirectoryString = currentDirectory.getAbsolutePath();
			if (selectedFile != null) {
				ss.selectedFileString = selectedFile.getAbsolutePath();
			} else {
				ss.selectedFileString = null;
			}
		}
		Log.e(TAG, "state saved");
		return superState;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		Log.e(TAG, "state restore");
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());
		if (ss.currentDirectoryString != null) {
			currentDirectory = new File(ss.currentDirectoryString);
		}
//		if (ss.selectedFileString != null) {
//			selectedFile = new File(ss.selectedFileString);
//		}
	}

	// Interfaces ///////////////////////////////////////////////////
	/**
	 * Interface called when ok or cancel are clicked
	 * 
	 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg,
	 *         apps@uwegerdes.de
	 */
	public interface OnFileSelectedListener {
		/**
		 * a file is selected and ok button clicked
		 * 
		 * @param file
		 *            selected file
		 */
		public void onFileSelected(File file);

		/**
		 * cancel button clicked or some other way used to end the view
		 */
		public void onCancel();
	}

	/**
	 * File selection changed - either to a file or to null
	 * 
	 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg,
	 *         apps@uwegerdes.de
	 */
	public interface OnSelectionChangeListener {
		/**
		 * callback when file selection changes - either to a file or to null
		 * 
		 * @param file
		 */
		public void onSelectionChanged(File file);

		/**
		 * callback when directory changes
		 * 
		 * @param file
		 */
		public void onDirectoryChanged(File directory);
	}

	// setting the listeners ////////////////////////////////////////
	/**
	 * set the listener for file selected ok / cancel
	 * 
	 * @param onFileSelectedListener
	 */
	public void setOnFileSelectedListener(
			OnFileSelectedListener onFileSelectedListener) {
		this.onFileSelectedListener = onFileSelectedListener;
	}

	/**
	 * set the listener for selection change events
	 * 
	 * @param onSelectionChangeListener
	 */
	public void setOnSelectionChangeListener(
			OnSelectionChangeListener onSelectionChangeListener) {
		setWidgetStates();
		this.onSelectionChangeListener = onSelectionChangeListener;
	}

	/**
	 * hide ok / cancel buttons - needed for dialog
	 */
	public void hideButtons() {
		View buttonContainer = findViewById(R.id.file_save_as_ButtonContainer);
		buttonContainer.setVisibility(View.GONE);
	}

	public boolean okEnabled() {
		File currentFile = getCurrentFile();
		if (currentFile != null) {
			if (currentFile.exists()) {
				if (file_save_as_Overwrite.isChecked()) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		}
		return false;
	}
	// private methods //////////////////////////////////////////////
	/**
	 * initialize parent and add content view
	 */
	private void initView() {
		if (parent == null) {
			if (getParent() != null) {
				parent = (ViewGroup) getParent();
			}
		}
		setSaveEnabled(true);
		setId(System.identityHashCode(this));
		if (getChildCount() == 0) {
			addView(getFileSelectorView());
		}
		setWidgetStates();
	}

	/**
	 * inflate the file_save_as__view and get the view element handles
	 * 
	 * @return view
	 */
	private View getFileSelectorView() {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View fileSelectorView = mInflater.inflate(R.layout.file_save_as_view,
				parent, false);
		if (baseDirectory == null
				|| baseDirectory.getAbsolutePath().length() < 3) {
			baseDirectory = Environment.getExternalStorageDirectory();
		}
		if (currentDirectory == null
				|| currentDirectory.getAbsolutePath().length() < baseDirectory
						.getAbsolutePath().length()) {
			currentDirectory = baseDirectory;
		}
		// get view handles
		file_save_as_Filename = (EditText) fileSelectorView
				.findViewById(R.id.file_save_as_Filename);
		file_save_as_Filetype = (TextView) fileSelectorView
				.findViewById(R.id.file_save_as_Filetype);
		file_save_as_Directory = (TextView) fileSelectorView
				.findViewById(R.id.file_save_as_Directory);
		file_save_as_ListView = (ListView) fileSelectorView
				.findViewById(R.id.file_save_as_ListView);
		file_save_as_Overwrite = (CheckBox) fileSelectorView
				.findViewById(R.id.file_save_as_Overwrite);
		file_save_as_ButtonUp = (ImageButton) fileSelectorView
				.findViewById(R.id.file_save_as_ButtonUp);
		file_save_as_ButtonOk = (Button) fileSelectorView
				.findViewById(R.id.file_save_as_ButtonOk);
		file_save_as_ButtonCancel = (Button) fileSelectorView
				.findViewById(R.id.file_save_as_ButtonCancel);

		// set some listeners and texts
		file_save_as_Filename.setText(filename);
		file_save_as_Filename.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable arg0) {
				updateView();
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
			}
		});
		file_save_as_Filetype.setText(filetype);

		file_save_as_Directory.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goUp();
			}
		});
		file_save_as_ButtonUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goUp();
			}
		});
		file_save_as_ButtonUp.setVisibility(View.GONE);

		file_save_as_Overwrite.setVisibility(View.GONE);
		file_save_as_Overwrite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//updateView();
			}
		});
		file_save_as_Overwrite.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				updateView();
			}
		});

		file_save_as_ButtonOk.setText(android.R.string.ok);
		file_save_as_ButtonOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				File selectedFile = getSelectedFile();
				if (selectedFile == null) {
					return;
				}
				if (onFileSelectedListener != null) {
					onFileSelectedListener.onFileSelected(selectedFile);
				} else if (context instanceof OnFileSelectedListener) {
					((OnFileSelectedListener) context)
							.onFileSelected(selectedFile);
				} else if (onOkClickCallback != null
						&& onOkClickCallback.length() > 0) {
					okClickCallback(selectedFile);
				} else {
					Log.e(TAG, "no OnFileSelectedListener in "
							+ getContext().getClass().toString());
				}
			}
		});
		file_save_as_ButtonOk.setEnabled(false);

		file_save_as_ButtonCancel.setText(android.R.string.cancel);
		file_save_as_ButtonCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onFileSelectedListener != null) {
					onFileSelectedListener.onCancel();
				} else if (context instanceof OnFileSelectedListener) {
					((OnFileSelectedListener) context).onCancel();
				} else if (onCancelClickCallback != null
						&& onCancelClickCallback.length() > 0) {
					cancelClickCallback();
				} else {
					Log.e(TAG, "no OnFileSelectedListener in "
							+ getContext().getClass().toString());
				}
			}
		});

		// set up listview
		file_save_as_ListAdapter = new FileSelectorAdapter(mInflater);
		if (currentDirectory != null && !currentDirectory.equals(baseDirectory)) {
			file_save_as_ListAdapter.setDirectory(currentDirectory);
		}
		file_save_as_ListView.setAdapter(file_save_as_ListAdapter);
		file_save_as_ListView.setOnItemClickListener(file_save_as_ListAdapter);

		return fileSelectorView;
	}

	/**
	 * 
	 */
	protected void updateView() {
		setWidgetStates();
		if (onSelectionChangeListener != null) {
			onSelectionChangeListener.onSelectionChanged(getSelectedFile());
		}
	}

	public void setWidgetStates() {
		File currentFile = getCurrentFile();
		if (currentFile != null) {
			if (currentFile.exists()) {
				file_save_as_Overwrite.setVisibility(View.VISIBLE);
				if (file_save_as_Overwrite.isChecked()) {
					file_save_as_ButtonOk.setEnabled(true);
				} else {
					file_save_as_ButtonOk.setEnabled(false);
				}
			} else {
				file_save_as_Overwrite.setVisibility(View.GONE);
				file_save_as_Overwrite.setChecked(false);
				file_save_as_ButtonOk.setEnabled(true);
			}
		} else {
			file_save_as_Overwrite.setVisibility(View.GONE);
			file_save_as_ButtonOk.setEnabled(false);
		}
	}

	protected File getCurrentFile() {
		File currentFile = null;
		filename = file_save_as_Filename.getText().toString();
		if (filename.length() > 0) {
			currentFile = new File(currentDirectory, filename + "." + filetype);
		}
		return currentFile;
	}

	private void okClickCallback(File file) {
		try {
			Method callback = getContext().getClass().getMethod(
					onOkClickCallback, new Class[] { File.class });
			if (callback != null) {
				callback.invoke(getContext(), file);
			}
		} catch (NoSuchMethodException e) {
			Log.e(TAG, "NoSuchMethodException " + e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "IllegalArgumentException " + e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			Log.e(TAG, "IllegalAccessException " + e.getLocalizedMessage());
		} catch (InvocationTargetException e) {
			Log.e(TAG, "InvocationTargetException " + e.getMessage());
		}
	}

	private void cancelClickCallback() {
		try {
			Method callback = getContext().getClass().getMethod(
					onCancelClickCallback, new Class[] {});
			if (callback != null) {
				callback.invoke(getContext());
			}
		} catch (NoSuchMethodException e) {
			Log.e(TAG, "NoSuchMethodException " + e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "IllegalArgumentException " + e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			Log.e(TAG, "IllegalAccessException " + e.getLocalizedMessage());
		} catch (InvocationTargetException e) {
			Log.e(TAG, "InvocationTargetException " + e.getMessage());
		}
	}

	// List adapter /////////////////////////////////////////////////
	/**
	 * adapter for directory item views
	 */
	public class FileSelectorAdapter extends BaseAdapter implements
			OnItemClickListener {
		public final static String TAG = "FileSelectorAdapter";

		private final LayoutInflater mInflater;
		private DirectoryReader directoryReader;
		private ArrayList<File> files;

		public FileSelectorAdapter(LayoutInflater mInflater) {
			this.mInflater = mInflater;
			this.directoryReader = new DirectoryReader(
					FileSelector.SELECT_TYPE_DIRECTORY, filetype, "");
			setDirectory();
		}

		@Override
		public int getCount() {
			return files.size();
		}

		@Override
		public File getItem(int position) {
			return files.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.fileselector_item,
						parent, false);
				ViewHolder holder = new ViewHolder();
				holder.filename = (TextView) convertView
						.findViewById(R.id.fileselector_item_Filename);
				holder.fileinfo = (TextView) convertView
						.findViewById(R.id.fileselector_item_FileInfo);
				holder.fileicon = (ImageView) convertView
						.findViewById(R.id.fileselector_item_FileIcon);
				holder.fileselected = (ImageView) convertView
						.findViewById(R.id.fileselector_item_SelectRadio);
				convertView.setTag(holder);
			}
			bindView(convertView, position);
			return convertView;
		}

		/**
		 * go up one directory but not further than baseDirectory
		 */
		public void goUp() {
			if (currentDirectory.getAbsolutePath().length() > baseDirectory
					.getAbsolutePath().length()
					&& currentDirectory.getAbsolutePath().startsWith(
							baseDirectory.getAbsolutePath())) {
				currentDirectory = currentDirectory.getParentFile();
				setDirectory();
			} else {
				Log.e(TAG, "goUp not possible");
			}
		}

		/**
		 * set the directory
		 * 
		 * @param directory
		 */
		public void setDirectory(File directory) {
			currentDirectory = directory;
			setDirectory();
		}

		private void bindView(View view, int position) {
			final File file = getItem(position);
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.filename.setText(file.getName());
			if (file.isDirectory()) {
				holder.fileinfo.setVisibility(View.GONE);
				holder.fileinfo.setText("");
				holder.fileicon.setVisibility(View.VISIBLE);
				holder.fileicon.setImageResource(R.drawable.ic_directory);
				holder.fileselected.setSelected(false);
				holder.fileselected.setVisibility(View.VISIBLE);
				holder.fileselected.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						setRadioState(v, file);
					}
				});
			} else {
				holder.fileinfo.setVisibility(View.VISIBLE);
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(file.lastModified());
				holder.fileinfo.setText(formatDate(calendar) + ", "
						+ formatSize(file.length()));
				if (file.getName().endsWith(".jpg")
						|| file.getName().endsWith(".jpeg")
						|| file.getName().endsWith(".png")) {
					holder.fileicon.setVisibility(View.VISIBLE);
					holder.fileicon
							.setImageResource(android.R.drawable.ic_menu_camera);
				} else {
					holder.fileicon.setVisibility(View.INVISIBLE);
					holder.fileicon.setImageDrawable(null);
				}
				holder.fileselected.setVisibility(View.GONE);
			}
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			File file = getItem(position);
			if (file.isDirectory()) {
				currentDirectory = file.getAbsoluteFile();
				setDirectory();
			} else {
				View radio = view
						.findViewById(R.id.fileselector_item_SelectRadio);
				setRadioState(radio, file);
			}
		}

		private void setRadioState(View radio, File file) {
			// TODO: radio state not used - copy filename to input field and set
			// selectedFile
			File selectedFile = getSelectedFile();
			Log.e(TAG, "copy filename to input field and set selectedFile");
			if (selectedFile != null) {
				radio.setSelected(false);
				selectedRadio = null;
				selectedFile = null;
				file_save_as_ButtonOk.setEnabled(false);
			} else {
				if (selectedRadio != null) {
					selectedRadio.setSelected(false);
				}
				radio.setSelected(true);
				selectedRadio = radio;
				selectedFile = file;
				file_save_as_ButtonOk.setEnabled(true);
			}
			setWidgetStates();
			if (onSelectionChangeListener != null) {
				onSelectionChangeListener.onSelectionChanged(selectedFile);
			}
		}

		private void setDirectory() {
			File selectedFile = getSelectedFile();
			if (selectedRadio != null) {
				selectedFile = null;
				selectedRadio.setSelected(false);
				selectedRadio = null;
			}
			file_save_as_ButtonOk.setEnabled(false);
			// notify dialog that no file is selected
			setWidgetStates();
			if (onSelectionChangeListener != null) {
				onSelectionChangeListener.onSelectionChanged(selectedFile);
				onSelectionChangeListener.onDirectoryChanged(currentDirectory);
			}
			if (file_save_as_Directory != null) {
				file_save_as_Directory.setText(currentDirectory
						.getAbsolutePath());
			} else {
				Log.e(TAG, "no directory view for: " + currentDirectory);
			}
			if (currentDirectory.equals(baseDirectory)) {
				file_save_as_ButtonUp.setVisibility(View.INVISIBLE);
			} else {
				file_save_as_ButtonUp.setVisibility(View.VISIBLE);
			}
			this.files = directoryReader.readDirectory(currentDirectory);
			notifyDataSetChanged();
		}
	}

	private static class ViewHolder {
		TextView filename;
		TextView fileinfo;
		ImageView fileicon;
		ImageView fileselected;
	}

	public void goUp() {
		file_save_as_ListAdapter.goUp();
	}

	/**
	 * retrieve attributes
	 * 
	 * @param attrs
	 */
	private void obtainAttributes(AttributeSet attrs) {
		if (attrs == null) {
			Log.e(TAG, "no attributes in FileSelectorView constructor");
			return;
		}
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.FileSelectorView);
		filename = a.getString(R.styleable.FileSelectorView_filename);
		filetype = a.getString(R.styleable.FileSelectorView_filetype);
		String basedir = a
				.getString(R.styleable.FileSelectorView_baseDirectory);
		if (basedir != null) {
			baseDirectory = new File(basedir);
		}
		String currDir = a
				.getString(R.styleable.FileSelectorView_currentDirectory);
		if (currDir != null) {
			currentDirectory = new File(currDir);
		}
		onOkClickCallback = a.getString(R.styleable.FileSelectorView_onOkClick);
		onCancelClickCallback = a
				.getString(R.styleable.FileSelectorView_onCancelClick);
		a.recycle();
	}

	/**
	 * format the calendar object to local string
	 * 
	 * @param date
	 * @return String with local date representation
	 */
	public static String formatDate(Calendar date) {
		String string = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
				DateFormat.MEDIUM).format(date.getTime());
		return string;
	}

	/**
	 * format the file size to a human readable string
	 * 
	 * @param size
	 * @return String with shortened size
	 */
	public static String formatSize(long size) {
		String retStr = "";
		long ten_times_value = 0;
		float displayValue = 0.f;
		String displayUnit = "";
		if (size <= 1024) {
			displayUnit = "bytes";
			retStr = String.format(Locale.getDefault(), "%d %s", size,
					displayUnit);
		} else {
			if (size > (1024 * 1024 * 1024)) {
				ten_times_value = size * 10 / (1024 * 1024 * 1024);
				displayUnit = "GB";
			} else if (size > (1024 * 1024)) {
				ten_times_value = size * 10 / (1024 * 1024);
				displayUnit = "MB";
			} else if (size > 1024) {
				ten_times_value = size * 10 / (1024);
				displayUnit = "kB";
			}
			displayValue = ten_times_value / 10.f;
			retStr = String.format(Locale.getDefault(), "%.1f %s",
					displayValue, displayUnit);
		}
		return retStr;
	}

	/**
	 * convenience method for testing
	 */
	public void setUpForTest() {
		initView();
	}

	/**
	 * get the selected file
	 * 
	 * @return File selected file
	 */
	public File getSelectedFile() {
		return getCurrentFile();
	}

	/**
	 * get the current directory
	 * 
	 * @return File directory
	 */
	public File getCurrentDirectory() {
		return currentDirectory;
	}

	/**
	 * @return
	 */
	public boolean isOverWriteChecked() {
		// TODO Auto-generated method stub
		return false;
	}
}
