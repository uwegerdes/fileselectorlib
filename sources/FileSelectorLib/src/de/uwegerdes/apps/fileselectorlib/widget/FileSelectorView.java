package de.uwegerdes.apps.fileselectorlib.widget;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.R;
import de.uwegerdes.apps.fileselectorlib.io.DirectoryReader;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Widget for file selection for given directory, extension or mime type list
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class FileSelectorView extends FrameLayout {
	public final static String TAG = "FileSelectorView";

	private Context context;
	private LayoutInflater mInflater;
	private ViewGroup parent;

	private File baseDirectory;
	private String selectType;
	private String extensions;
	private String mimeTypes;
	private File currentDirectory;
	private File selectedFile = null;
	private String onOkClickCallback;
	private String onCancelClickCallback;

	private TextView fileselectorDirectory;
	private ListView fileselectorListView;
	private FileSelectorAdapter fileselectorListAdapter;
	private ImageButton fileselectorButtonUp;
	private Button fileselectorButtonOk;
	private Button fileselectorButtonCancel;
	private OnFileSelectedListener onFileSelectedListener;
	private OnSelectionChangeListener onSelectionChangeListener;

	private View selectedRadio = null;

	/**
	 * @param context
	 */
	public FileSelectorView(Context context) {
		super(context);
		this.context = getContext();
		this.selectType = FileSelector.SELECT_TYPE_FILE;
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public FileSelectorView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = getContext();
		obtainAttributes(attrs);
		initView();
	}

	/**
	 * @param context
	 */
	public FileSelectorView(Context context, String selectType,
			String extensions, String mimeTypes, String baseDirectory) {
		super(context);
		this.context = getContext();
		this.selectType = selectType;
		this.extensions = extensions;
		this.mimeTypes = mimeTypes;
		this.baseDirectory = new File(baseDirectory);
		this.currentDirectory = new File(baseDirectory);
		initView();
	}

	/**
	 * @param context
	 * @param selectedFile
	 */
	public FileSelectorView(Context context, String selectType,
			String extensions, String mimeTypes, String baseDirectory,
			String currentDirectory, String selectedFile) {
		super(context);
		this.context = getContext();
		this.selectType = selectType;
		this.extensions = extensions;
		this.mimeTypes = mimeTypes;
		this.baseDirectory = new File(baseDirectory);
		if (currentDirectory != null) {
			this.currentDirectory = new File(currentDirectory);
		} else {
			this.currentDirectory = new File(baseDirectory);
		}
		if (selectedFile != null && selectedFile.startsWith(currentDirectory)) {
			this.selectedFile = new File(selectedFile);
		}
		initView();
	}

	// Save state ///////////////////////////////////////////////////
	public class SavedState extends BaseSavedState {
		String currentDirectoryString;
		String selectedFileString;

		public SavedState(Parcelable superState) {
			super(superState);
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeString(currentDirectoryString);
			out.writeString(selectedFileString);
		}

		@Override
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append("FileSelectorView.SavedState{");
			buffer.append(Integer.toHexString(System.identityHashCode(this)));
			buffer.append(" currentDirectory=" + currentDirectoryString);
			buffer.append(" selectedFile=" + selectedFileString);
			buffer.append("}");
			return buffer.toString();
		}

		public final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};

		private SavedState(Parcel in) {
			super(in);
			currentDirectoryString = in.readString();
			selectedFileString = in.readString();
		}
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		if (currentDirectory != baseDirectory || selectedFile != null) {
			SavedState ss = new SavedState(superState);
			// TODO: Should also save the current scroll position!
			ss.currentDirectoryString = currentDirectory.getAbsolutePath();
			if (selectedFile != null) {
				ss.selectedFileString = selectedFile.getAbsolutePath();
			} else {
				ss.selectedFileString = null;
			}
		}
		Log.e(TAG, "state saved");
		return superState;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		Log.e(TAG, "state restore");
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());
		if (ss.currentDirectoryString != null) {
			currentDirectory = new File(ss.currentDirectoryString);
		}
		if (ss.selectedFileString != null) {
			selectedFile = new File(ss.selectedFileString);
		}
	}

	// Interfaces ///////////////////////////////////////////////////
	/**
	 * Interface called when ok or cancel are clicked
	 * 
	 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg,
	 *         apps@uwegerdes.de
	 */
	public interface OnFileSelectedListener {
		/**
		 * a file is selected and ok button clicked
		 * 
		 * @param file
		 *            selected file
		 */
		public void onFileSelected(File file);

		/**
		 * cancel button clicked or some other way used to end the view
		 */
		public void onCancel();
	}

	/**
	 * File selection changed - either to a file or to null
	 * 
	 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg,
	 *         apps@uwegerdes.de
	 */
	public interface OnSelectionChangeListener {
		/**
		 * callback when file selection changes - either to a file or to null
		 * 
		 * @param file
		 */
		public void onSelectionChanged(File file);

		/**
		 * callback when directory changes
		 * 
		 * @param file
		 */
		public void onDirectoryChanged(File directory);
	}

	// setting the listeners ////////////////////////////////////////
	/**
	 * set the listener for file selected ok / cancel
	 * 
	 * @param onFileSelectedListener
	 */
	public void setOnFileSelectedListener(
			OnFileSelectedListener onFileSelectedListener) {
		this.onFileSelectedListener = onFileSelectedListener;
	}

	/**
	 * set the listener for selection change events
	 * 
	 * @param onSelectionChangeListener
	 */
	public void setOnSelectionChangeListener(
			OnSelectionChangeListener onSelectionChangeListener) {
		this.onSelectionChangeListener = onSelectionChangeListener;
	}

	/**
	 * hide ok / cancel buttons - needed for dialog
	 */
	public void hideButtons() {
		View buttonContainer = findViewById(R.id.fileselector_view_Button_Container);
		buttonContainer.setVisibility(View.GONE);
	}

	// private methods //////////////////////////////////////////////
	/**
	 * initialize parent and add content view
	 */
	private void initView() {
		if (parent == null) {
			if (getParent() != null) {
				parent = (ViewGroup) getParent();
			}
		}
		setSaveEnabled(true);
		setId(System.identityHashCode(this));
		if (getChildCount() == 0) {
			addView(getFileSelectorView());
		}
	}

	/**
	 * inflate the fileselector_view and get the view element handles
	 * 
	 * @return view
	 */
	private View getFileSelectorView() {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View fileSelectorView = mInflater.inflate(R.layout.fileselector_view,
				parent, false);
		if (baseDirectory == null
				|| baseDirectory.getAbsolutePath().length() < 3) {
			baseDirectory = Environment.getExternalStorageDirectory();
		}
		if (currentDirectory == null
				|| currentDirectory.getAbsolutePath().length() < baseDirectory
						.getAbsolutePath().length()) {
			currentDirectory = baseDirectory;
		}
		// get view handles
		fileselectorDirectory = (TextView) fileSelectorView
				.findViewById(R.id.fileselector_view_Directory);
		fileselectorListView = (ListView) fileSelectorView
				.findViewById(R.id.fileselector_view_ListView);
		fileselectorButtonUp = (ImageButton) fileSelectorView
				.findViewById(R.id.fileselector_view_ButtonUp);
		fileselectorButtonOk = (Button) fileSelectorView
				.findViewById(R.id.fileselector_view_ButtonOk);
		fileselectorButtonCancel = (Button) fileSelectorView
				.findViewById(R.id.fileselector_view_ButtonCancel);

		// set some listeners and texts
		fileselectorDirectory.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goUp();
			}
		});
		fileselectorButtonUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goUp();
			}
		});
		fileselectorButtonUp.setVisibility(View.GONE);

		fileselectorButtonOk.setText(android.R.string.ok);
		fileselectorButtonOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (selectedFile == null) {
					return;
				}
				if (onFileSelectedListener != null) {
					onFileSelectedListener.onFileSelected(selectedFile);
				} else if (context instanceof OnFileSelectedListener) {
					((OnFileSelectedListener) context)
							.onFileSelected(selectedFile);
				} else if (onOkClickCallback != null
						&& onOkClickCallback.length() > 0) {
					okClickCallback(selectedFile);
				} else {
					Log.e(TAG, "no OnFileSelectedListener in "
							+ getContext().getClass().toString());
				}
			}
		});
		fileselectorButtonOk.setEnabled(false);

		fileselectorButtonCancel.setText(android.R.string.cancel);
		fileselectorButtonCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onFileSelectedListener != null) {
					onFileSelectedListener.onCancel();
				} else if (context instanceof OnFileSelectedListener) {
					((OnFileSelectedListener) context).onCancel();
				} else if (onCancelClickCallback != null
						&& onCancelClickCallback.length() > 0) {
					cancelClickCallback();
				} else {
					Log.e(TAG, "no OnFileSelectedListener in "
							+ getContext().getClass().toString());
				}
			}
		});

		// set up listview
		fileselectorListAdapter = new FileSelectorAdapter(mInflater);
		if (currentDirectory != null && !currentDirectory.equals(baseDirectory)) {
			fileselectorListAdapter.setDirectory(currentDirectory);
		}
		fileselectorListView.setAdapter(fileselectorListAdapter);
		fileselectorListView.setOnItemClickListener(fileselectorListAdapter);

		return fileSelectorView;
	}

	private void okClickCallback(File file) {
		try {
			Method callback = getContext().getClass().getMethod(onOkClickCallback,
					new Class[] { File.class });
			if (callback != null) {
				callback.invoke(getContext(), file);
			}
		} catch (NoSuchMethodException e) {
			Log.e(TAG, "NoSuchMethodException " + e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "IllegalArgumentException " + e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			Log.e(TAG, "IllegalAccessException " + e.getLocalizedMessage());
		} catch (InvocationTargetException e) {
			Log.e(TAG, "InvocationTargetException " + e.getMessage());
		}
	}

	private void cancelClickCallback() {
		try {
			Method callback = getContext().getClass().getMethod(onCancelClickCallback,
					new Class[] { });
			if (callback != null) {
				callback.invoke(getContext());
			}
		} catch (NoSuchMethodException e) {
			Log.e(TAG, "NoSuchMethodException " + e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "IllegalArgumentException " + e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			Log.e(TAG, "IllegalAccessException " + e.getLocalizedMessage());
		} catch (InvocationTargetException e) {
			Log.e(TAG, "InvocationTargetException " + e.getMessage());
		}
	}

	// List adapter /////////////////////////////////////////////////
	/**
	 * adapter for directory item views
	 */
	public class FileSelectorAdapter extends BaseAdapter implements
			OnItemClickListener {
		public final static String TAG = "FileSelectorAdapter";

		private final LayoutInflater mInflater;
		private DirectoryReader directoryReader;
		private ArrayList<File> files;

		public FileSelectorAdapter(LayoutInflater mInflater) {
			this.mInflater = mInflater;
			this.directoryReader = new DirectoryReader(selectType, extensions,
					mimeTypes);
			setDirectory();
		}

		@Override
		public int getCount() {
			return files.size();
		}

		@Override
		public File getItem(int position) {
			return files.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.fileselector_item,
						parent, false);
				ViewHolder holder = new ViewHolder();
				holder.filename = (TextView) convertView
						.findViewById(R.id.fileselector_item_Filename);
				holder.fileinfo = (TextView) convertView
						.findViewById(R.id.fileselector_item_FileInfo);
				holder.fileicon = (ImageView) convertView
						.findViewById(R.id.fileselector_item_FileIcon);
				holder.fileselected = (ImageView) convertView
						.findViewById(R.id.fileselector_item_SelectRadio);
				convertView.setTag(holder);
			}
			bindView(convertView, position);
			return convertView;
		}

		/**
		 * go up one directory but not further than baseDirectory
		 */
		public void goUp() {
			if (currentDirectory.getAbsolutePath().length() > baseDirectory
					.getAbsolutePath().length()
					&& currentDirectory.getAbsolutePath().startsWith(
							baseDirectory.getAbsolutePath())) {
				currentDirectory = currentDirectory.getParentFile();
				setDirectory();
			} else {
				Log.e(TAG, "goUp not possible");
			}
		}

		/**
		 * set the directory
		 * 
		 * @param directory
		 */
		public void setDirectory(File directory) {
			currentDirectory = directory;
			setDirectory();
		}

		private void bindView(View view, int position) {
			final File file = getItem(position);
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.filename.setText(file.getName());
			if (file.isDirectory()) {
				holder.fileinfo.setVisibility(View.GONE);
				holder.fileinfo.setText("");
				holder.fileicon.setVisibility(View.VISIBLE);
				holder.fileicon.setImageResource(R.drawable.ic_directory);
				if (selectType.equals(FileSelector.SELECT_TYPE_DIRECTORY)) {
					holder.fileselected.setSelected(false);
					holder.fileselected.setVisibility(View.VISIBLE);
					holder.fileselected
							.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									setRadioState(v, file);
								}
							});
				} else {
					holder.fileselected.setVisibility(View.GONE);
					holder.fileselected.setOnClickListener(null);
				}
			} else {
				holder.fileinfo.setVisibility(View.VISIBLE);
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(file.lastModified());
				holder.fileinfo.setText(formatDate(calendar) + ", "
						+ formatSize(file.length()));
				if (file.getName().endsWith(".jpg")
						|| file.getName().endsWith(".jpeg")
						|| file.getName().endsWith(".png")) {
					holder.fileicon.setVisibility(View.VISIBLE);
					holder.fileicon
							.setImageResource(android.R.drawable.ic_menu_camera);
				} else {
					holder.fileicon.setVisibility(View.INVISIBLE);
					holder.fileicon.setImageDrawable(null);
				}
				holder.fileselected.setSelected(false);
				holder.fileselected.setVisibility(View.VISIBLE);
				holder.fileselected.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						setRadioState(v, file);
					}
				});
			}
			if (selectedFile != null && selectedFile.equals(file)) {
				holder.fileselected.setSelected(true);
				fileselectorButtonOk.setEnabled(true);
			}
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			File file = getItem(position);
			if (file.isDirectory()) {
				currentDirectory = file.getAbsoluteFile();
				setDirectory();
				fileselectorListView.setSelectionFromTop(0, 0);
			} else {
				View radio = view
						.findViewById(R.id.fileselector_item_SelectRadio);
				setRadioState(radio, file);
			}
		}

		private void setRadioState(View radio, File file) {
			if (selectedFile != null && selectedFile.equals(file)) {
				radio.setSelected(false);
				selectedRadio = null;
				selectedFile = null;
				fileselectorButtonOk.setEnabled(false);
			} else {
				if (selectedRadio != null) {
					selectedRadio.setSelected(false);
				}
				radio.setSelected(true);
				selectedRadio = radio;
				selectedFile = file;
				fileselectorButtonOk.setEnabled(true);
			}
			if (onSelectionChangeListener != null) {
				onSelectionChangeListener.onSelectionChanged(selectedFile);
			}
		}

		private void setDirectory() {
			if (selectedRadio != null) {
				selectedFile = null;
				selectedRadio.setSelected(false);
				selectedRadio = null;
			}
			fileselectorButtonOk.setEnabled(false);
			// notify dialog that no file is selected
			if (onSelectionChangeListener != null) {
				onSelectionChangeListener.onSelectionChanged(selectedFile);
				onSelectionChangeListener.onDirectoryChanged(currentDirectory);
			}
			if (fileselectorDirectory != null) {
				fileselectorDirectory.setText(currentDirectory
						.getAbsolutePath());
			} else {
				Log.e(TAG, "no directory view for: " + currentDirectory);
			}
			if (currentDirectory.equals(baseDirectory)) {
				fileselectorButtonUp.setVisibility(View.INVISIBLE);
			} else {
				fileselectorButtonUp.setVisibility(View.VISIBLE);
			}
			this.files = directoryReader.readDirectory(currentDirectory);
			notifyDataSetChanged();
		}
	}

	private static class ViewHolder {
		TextView filename;
		TextView fileinfo;
		ImageView fileicon;
		ImageView fileselected;
	}

	public void goUp() {
		fileselectorListAdapter.goUp();
	}

	/**
	 * retrieve attributes
	 * 
	 * @param attrs
	 */
	private void obtainAttributes(AttributeSet attrs) {
		if (attrs == null) {
			Log.e(TAG, "no attributes in FileSelectorView constructor");
			return;
		}
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.FileSelectorView);
		selectType = a.getString(R.styleable.FileSelectorView_selectType);
		extensions = a.getString(R.styleable.FileSelectorView_extensions);
		mimeTypes = a.getString(R.styleable.FileSelectorView_mimeTypes);
		String basedir = a
				.getString(R.styleable.FileSelectorView_baseDirectory);
		if (basedir != null) {
			baseDirectory = new File(basedir);
		}
		String currDir = a
				.getString(R.styleable.FileSelectorView_currentDirectory);
		if (currDir != null) {
			currentDirectory = new File(currDir);
		}
		String selFile = a.getString(R.styleable.FileSelectorView_selectedFile);
		if (selFile != null) {
			selectedFile = new File(selFile);
		}
		onOkClickCallback = a.getString(R.styleable.FileSelectorView_onOkClick);
		onCancelClickCallback = a.getString(R.styleable.FileSelectorView_onCancelClick);
		a.recycle();
	}

	/**
	 * format the calendar object to local string
	 * 
	 * @param date
	 * @return String with local date representation
	 */
	public static String formatDate(Calendar date) {
		String string = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
				DateFormat.MEDIUM).format(date.getTime());
		return string;
	}

	/**
	 * format the file size to a human readable string
	 * 
	 * @param size
	 * @return String with shortened size
	 */
	public static String formatSize(long size) {
		String retStr = "";
		long ten_times_value = 0;
		float displayValue = 0.f;
		String displayUnit = "";
		if (size <= 1024) {
			displayUnit = "bytes";
			retStr = String.format(Locale.getDefault(), "%d %s", size,
					displayUnit);
		} else {
			if (size > (1024 * 1024 * 1024)) {
				ten_times_value = size * 10 / (1024 * 1024 * 1024);
				displayUnit = "GB";
			} else if (size > (1024 * 1024)) {
				ten_times_value = size * 10 / (1024 * 1024);
				displayUnit = "MB";
			} else if (size > 1024) {
				ten_times_value = size * 10 / (1024);
				displayUnit = "kB";
			}
			displayValue = ten_times_value / 10.f;
			retStr = String.format(Locale.getDefault(), "%.1f %s",
					displayValue, displayUnit);
		}
		return retStr;
	}

	/**
	 * convenience method for testing
	 */
	public void setUpForTest() {
		initView();
	}

	/**
	 * get the selected file
	 * 
	 * @return File selected file
	 */
	public File getSelectedFile() {
		return selectedFile;
	}

	/**
	 * get the current directory
	 * 
	 * @return File directory
	 */
	public File getCurrentDirectory() {
		return currentDirectory;
	}
}
