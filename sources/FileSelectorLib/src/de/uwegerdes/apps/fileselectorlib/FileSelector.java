package de.uwegerdes.apps.fileselectorlib;

/**
 * Constants for FileSelector - keep in sync with attrs.xml
 * 
 * @author uwe
 */
public final class FileSelector {
	// Labels for configuration attributes
	public static final String TITLE = "title";
	public static final String SELECT_TYPE = "selectType";
	public static final String EXTENSIONS = "extensions";
	public static final String MIMETYPES = "mimeTypes";
	public static final String BASE_DIRECTORY = "baseDirectory";
	public static final String CURRENT_DIRECTORY = "currentDirectory";
	public static final String FILENAME = "filename";
	public static final String FILETYPE = "filetype";

	// Labels for internal attributes
	public static final String SELECTED_FILE = "selectedFile";
	public static final String ON_OK_CLICK = "onOkClick";
	public static final String ON_CANCEL_CLICK = "onCancelClick";

	// String constants for selectType
	public static final String SELECT_TYPE_DIRECTORY = "directory";
	public static final String SELECT_TYPE_FILE = "file";
}
