/**
 * 
 */
package de.uwegerdes.apps.fileselectorsampleapp.test;

import java.io.File;

import com.jayway.android.robotium.solo.Solo;

import de.uwegerdes.apps.fileselectorlib.app.FileSelectorDialogFragment;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.FileSelectorAdapter;
import de.uwegerdes.apps.fileselectorsampleapp.MainActivity;
import de.uwegerdes.apps.fileselectorsampleapp.R;
import android.app.AlertDialog;
import android.app.Instrumentation;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class TestFileSelectorDialog extends
		ActivityInstrumentationTestCase2<MainActivity> {

	public final static String TAG = "TestFileSelectorSampleApp";

	private MainActivity activity;
	private Instrumentation instrumentation;
	private Solo solo;

	private String rootDir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String subDir = "FileSelectorSampleApp";
	private String filename = "test_file.txt";

	@SuppressWarnings("deprecation")
	public TestFileSelectorDialog() {
		super("de.uwegerdes.apps.fileselectorsampleapp", MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		activity = getActivity();
		instrumentation = getInstrumentation();
		solo = new Solo(instrumentation, activity);
		instrumentation.waitForIdleSync();
	}

	@Override
	protected void tearDown() throws Exception {
		solo.goBack();
		activity.finish();
		super.tearDown();
	}

	public void testStep10SelectDialog() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectDialog)));
		solo.clickOnText(activity.getString(R.string.buttonSelectDialog));
		instrumentation.waitForIdleSync();
		solo.waitForText(rootDir, 1, 50l);
		solo.scrollToTop();
		assertTrue(solo.searchText(rootDir));

		// check dialog
		FileSelectorDialogFragment dialogFragment = activity
				.getFileSelectorDialogFragment();
		AlertDialog dialog = dialogFragment.getAlertDialog();
		View dialogOkButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
		assertNotNull(dialogOkButton);
		assertFalse(dialogOkButton.isEnabled());
		View dialogCancelButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
		assertNotNull(dialogCancelButton);
		assertTrue(dialogCancelButton.isEnabled());
		// check title view
		TextView fileselectorDirectory = (TextView) dialog
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(fileselectorDirectory);
		String fileselectorDirectoryString = fileselectorDirectory.getText()
				.toString();
		assertEquals(rootDir, fileselectorDirectoryString);
		// check list content (only directories)
		ListView fileselectorListView = (ListView) dialog
				.findViewById(R.id.fileselector_view_ListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		LinearLayout buttonContainer = (LinearLayout) dialog
				.findViewById(R.id.fileselector_view_Button_Container);
		assertNotNull(buttonContainer);
		assertEquals(buttonContainer.getVisibility(), View.GONE);
		// check ok/cancel button
		int maxIndex = (fileselectorListView.getChildCount() < 5) ? fileselectorListView
				.getChildCount() : 5;
		for (int i = 0; i < maxIndex; i++) {
			solo.scrollToTop();
			File file = fileselectorListAdapter.getItem(i);
			Log.e(TAG, "file: " + file.getAbsolutePath().toString());
			assertTrue(solo.searchText(file.getName()));
			if (file.isDirectory()) {
				solo.clickOnText(file.getName());
				instrumentation.waitForIdleSync();
				solo.waitForFragmentByTag("wait a bit", 50);
				FileSelectorAdapter adapter = (FileSelectorAdapter) fileselectorListView
						.getAdapter();
				for (int j = 0; j < adapter.getCount(); j++) {
//					Log.e(TAG, "adapter: "
//							+ adapter.getItem(j).getAbsolutePath().toString());
					assertEquals(file.getAbsolutePath(), adapter.getItem(j)
							.getParent());
				}
				solo.scrollToTop();
				solo.clickOnView(dialog
						.findViewById(R.id.fileselector_view_Directory));
				instrumentation.waitForIdleSync();
				solo.waitForFragmentByTag("wait a bit", 50);
				assertFalse(dialogOkButton.isEnabled());
			} else {
				View imageView = fileselectorListView.getChildAt(i)
						.findViewById(R.id.fileselector_item_SelectRadio);
				assertFalse(file.getName(), imageView.isSelected());
				solo.clickOnText(file.getName());
				instrumentation.waitForIdleSync();
				solo.waitForFragmentByTag("wait a bit", 50);
				assertTrue(file.getName(), imageView.isSelected());
				assertTrue(dialogOkButton.isEnabled());
			}
		}

		solo.clickOnText(subDir);
		solo.clickOnText(filename);
		solo.clickOnButton(activity.getString(android.R.string.ok));
		String target = rootDir + "/" + subDir + "/" + filename;
		assertTrue("save file: " + target, solo.searchText(target));
	}
}
