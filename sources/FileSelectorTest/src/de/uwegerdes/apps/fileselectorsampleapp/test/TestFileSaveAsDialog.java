package de.uwegerdes.apps.fileselectorsampleapp.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.jayway.android.robotium.solo.Solo;

import de.uwegerdes.apps.fileselectorlib.app.FileSaveAsDialogFragment;
import de.uwegerdes.apps.fileselectorsampleapp.MainActivity;
import de.uwegerdes.apps.fileselectorsampleapp.R;
import android.app.AlertDialog;
import android.app.Instrumentation;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * testing file save as dialog
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class TestFileSaveAsDialog extends
		ActivityInstrumentationTestCase2<MainActivity> {
	public final static String TAG = "TestFileSaveAsDialog";

	private MainActivity activity;
	private Instrumentation instrumentation;
	private Solo solo;

	private String rootDir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String subDir = "FileSelectorSampleApp";
	private String filename1 = "sample_file";
	private String filename2 = "test_file";
	private String filetype = "txt";
	private File directory;
	private File testFile2;

	public TestFileSaveAsDialog() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		activity = getActivity();
		instrumentation = getInstrumentation();
		solo = new Solo(instrumentation, activity);
		instrumentation.waitForIdleSync();

	}

	@Override
	protected void tearDown() throws Exception {
		deleteTestFile(filename1);
		createTestFile(filename2);
		solo.goBack();
		activity.finish();
		super.tearDown();
	}

	public void testStep10OpenFileSaveAsDialog() {
		deleteTestFile(filename1);
		createTestFile(filename2);
		assertNotNull(activity);
		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectFileSaveAsDialog)));
		solo.clickOnText(activity
				.getString(R.string.buttonSelectFileSaveAsDialog));
		instrumentation.waitForIdleSync();

		String overwriteString = activity
				.getString(R.string.file_save_as_Overwrite);
		assertNotNull(overwriteString);
		assertTrue(overwriteString.length() > 0);
		// check view elements
		assertTrue(solo.searchText("File save as"));
		assertTrue(solo.searchText(filename1));
		assertTrue(solo.searchText(filetype));
		assertTrue(solo.searchText(rootDir));
		assertTrue(solo.searchText(subDir));
		assertFalse(solo.searchText(overwriteString, true));

		// check dialog buttons
		FileSaveAsDialogFragment dialogFragment = activity
				.getFileSaveAsDialogFragment();
		AlertDialog dialog = dialogFragment.getAlertDialog();
		View dialogOkButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
		assertNotNull(dialogOkButton);
		assertTrue(dialogOkButton.isEnabled());
		View dialogCancelButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
		assertNotNull(dialogCancelButton);
		assertTrue(dialogCancelButton.isEnabled());

		// check title view
		TextView file_save_as_Directory = (TextView) dialog
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.file_save_as_Directory);
		assertNotNull(file_save_as_Directory);
		String file_save_as_DirectoryString = file_save_as_Directory.getText()
				.toString();
		assertEquals(rootDir + "/" + subDir, file_save_as_DirectoryString);

		// check button ok disabled when no filename
		solo.clearEditText(0);
		solo.sendKey(KeyEvent.KEYCODE_CLEAR);
		assertFalse(solo.searchText(overwriteString, true));
		assertFalse(dialogOkButton.isEnabled());

		// enter test filename
		solo.enterText(0, filename2);
		solo.sendKey(KeyEvent.KEYCODE_DPAD_LEFT);
		instrumentation.waitForIdleSync();
		assertTrue(testFile2.exists());
		// solo.waitForFragmentByTag("wait a bit", 100);
		assertTrue(solo.searchText(overwriteString, true));
		assertFalse(dialogOkButton.isEnabled());
		solo.clickOnText(overwriteString);
		solo.searchText("wait a bit");
		assertTrue(dialogOkButton.isEnabled());

		// view button disabled
		LinearLayout buttonContainer = (LinearLayout) dialog
				.findViewById(R.id.file_save_as_ButtonContainer);
		assertNotNull(buttonContainer);
		assertEquals(buttonContainer.getVisibility(), View.GONE);

		// click ok and check result
		solo.clickOnButton(activity.getString(android.R.string.ok));
		String target = rootDir + "/" + subDir + "/" + filename2 + "."
				+ filetype;
		assertTrue("save file: " + target, solo.searchText(target));

	}

	public void testStep20OpenWithExistingFile() {
		createTestFile(filename1);
		deleteTestFile(filename2);
		assertNotNull(activity);
		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectFileSaveAsDialog)));
		solo.clickOnText(activity
				.getString(R.string.buttonSelectFileSaveAsDialog));
		instrumentation.waitForIdleSync();

		String overwriteString = activity
				.getString(R.string.file_save_as_Overwrite);
		assertNotNull(overwriteString);
		assertTrue(overwriteString.length() > 0);
		// check view elements
		assertTrue(solo.searchText("File save as"));
		assertTrue(solo.searchText(filename1));
		assertTrue(solo.searchText(filetype));
		assertTrue(solo.searchText(rootDir));
		assertTrue(solo.searchText(subDir));
		assertTrue(solo.searchText(overwriteString, true));

		// check dialog buttons
		FileSaveAsDialogFragment dialogFragment = activity
				.getFileSaveAsDialogFragment();
		AlertDialog dialog = dialogFragment.getAlertDialog();
		View dialogOkButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
		assertNotNull(dialogOkButton);
		assertFalse(dialogOkButton.isEnabled());
		View dialogCancelButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
		assertNotNull(dialogCancelButton);
		assertTrue(dialogCancelButton.isEnabled());

		// check title view
		TextView file_save_as_Directory = (TextView) dialog
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.file_save_as_Directory);
		assertNotNull(file_save_as_Directory);
		String file_save_as_DirectoryString = file_save_as_Directory.getText()
				.toString();
		assertEquals(rootDir + "/" + subDir, file_save_as_DirectoryString);

		// check button ok disabled when no filename
		solo.clearEditText(0);
		solo.sendKey(KeyEvent.KEYCODE_CLEAR);
		assertFalse(solo.searchText(overwriteString, true));
		assertFalse(dialogOkButton.isEnabled());

		// enter test filename
		solo.enterText(0, filename2);
		solo.sendKey(KeyEvent.KEYCODE_DPAD_RIGHT);
		instrumentation.waitForIdleSync();
		assertFalse(testFile2.exists());
		assertFalse(solo.searchText(overwriteString, true));
		assertTrue(dialogOkButton.isEnabled());

		// view button disabled
		LinearLayout buttonContainer = (LinearLayout) dialog
				.findViewById(R.id.file_save_as_ButtonContainer);
		assertNotNull(buttonContainer);
		assertEquals(buttonContainer.getVisibility(), View.GONE);

		// click ok and check result
		solo.clickOnButton(activity.getString(android.R.string.ok));
		String target = rootDir + "/" + subDir + "/" + filename2 + "."
				+ filetype;
		assertTrue("save file: " + target, solo.searchText(target));
		deleteTestFile(filename1);

	}

	private void createTestFile(String filename) {
		directory = new File(rootDir + "/" + subDir);
		testFile2 = new File(directory, filename + "." + filetype);
		if (testFile2.exists()) {
			return;
		}
		if (!directory.exists()) {
			if (!directory.mkdirs()) {
				Log.e(TAG, "failed to create " + directory.getPath());
			}
		}

		String content2 = "testcontent\n\nonly used for test";
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(testFile2);
		fileWriter.append(content2);
		} catch (IOException e) {
			Log.e(TAG, e.getLocalizedMessage());
		} finally {
			try {
				if (fileWriter != null) {
					fileWriter.close();
				}
			} catch (IOException e) {
				Log.e(TAG, e.getLocalizedMessage());
			}
		}
	}

	private void deleteTestFile(String filename) {
		directory = new File(rootDir + "/" + subDir);
		testFile2 = new File(directory, filename + "." + filetype);
		if (testFile2.exists()) {
			testFile2.delete();
			if (!testFile2.delete()) {
				Log.e(TAG, "failed to delete: " + testFile2.getPath());
			}
		}
	}
}
