/**
 * 
 */
package de.uwegerdes.apps.fileselectorsampleapp.test;

import java.io.File;

import com.jayway.android.robotium.solo.Solo;

import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.FileSelectorAdapter;
import de.uwegerdes.apps.fileselectorsampleapp.MainActivity;
import de.uwegerdes.apps.fileselectorsampleapp.R;
import android.app.Instrumentation;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class TestViewUsageActivity extends
		ActivityInstrumentationTestCase2<MainActivity> {
	public final static String TAG = "TestViewUsageActivity";

	private MainActivity activity;
	private Instrumentation instrumentation;
	private Solo solo;

	private String activityName;
	private String rootDir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String subDir = "FileSelectorSampleApp";
	private String filename = "test_file.txt";

	@SuppressWarnings("deprecation")
	public TestViewUsageActivity() {
		super("de.uwegerdes.apps.fileselectorsampleapp", MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		activity = getActivity();
		instrumentation = getInstrumentation();
		solo = new Solo(instrumentation, activity);
		instrumentation.waitForIdleSync();
		activityName = activity.getString(R.string.view_usage_activity_name);
	}

	@Override
	protected void tearDown() throws Exception {
		solo.goBack();
		activity.finish();
		super.tearDown();
	}

	public void testStep10ViewElements() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectViewUsage)));
		solo.clickOnText(activity.getString(R.string.buttonSelectViewUsage));
		instrumentation.waitForIdleSync();
		solo.waitForFragmentByTag("wait a bit", 50);
		assertTrue(solo.searchText(activityName));
		assertTrue(solo.searchText(rootDir));

		// check title view
		TextView title = (TextView) solo
				.getCurrentActivity()
				.findViewById(
						de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(title);
		String titleString = title.getText().toString();
		assertEquals(rootDir, titleString);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());
		// select subdirectory
		// for (int i = 0; i < fileselectorListAdapter.getCount(); i++) {
		int maxIndex = (fileselectorListView.getChildCount() < 5) ? fileselectorListView
				.getChildCount() : 5;
		// maxIndex = -1;
		for (int i = 0; i < maxIndex; i++) {
			solo.scrollToTop();
			File file = fileselectorListAdapter.getItem(i);
			Log.e(TAG, file.getAbsolutePath().toString());
			assertTrue(solo.searchText(file.getName()));
			solo.scrollToTop();
			if (file.isDirectory()) {
				View itemView = fileselectorListView.getChildAt(i);
				View nameView = itemView
						.findViewById(R.id.fileselector_item_Filename);
				solo.clickOnView(nameView);
				instrumentation.waitForIdleSync();
				solo.waitForFragmentByTag("wait a bit", 50);
				FileSelectorAdapter adapter = (FileSelectorAdapter) fileselectorListView
						.getAdapter();
				for (int j = 0; j < adapter.getCount(); j++) {
					// Log.e(TAG, " . "
					// + adapter.getItem(j).getAbsolutePath().toString());
					assertEquals(file.getAbsolutePath(), adapter.getItem(j)
							.getParent());
				}
				solo.scrollToTop();
				solo.clickOnView(solo.getCurrentActivity().findViewById(
						R.id.fileselector_view_Directory));
				instrumentation.waitForIdleSync();
				solo.waitForFragmentByTag("wait a bit", 50);
				assertFalse(fileselectorButtonOk.isEnabled());
			} else {
				View imageView = fileselectorListView.getChildAt(i)
						.findViewById(R.id.fileselector_item_SelectRadio);
				assertFalse(file.getName(), imageView.isSelected());
				solo.clickOnText(file.getName());
				instrumentation.waitForIdleSync();
				solo.clickOnButton(activity.getString(android.R.string.ok));
				assertTrue(
						file.getAbsolutePath(),
						solo.searchText("ok button clicked: "
								+ file.getAbsolutePath()));
				solo.scrollToTop();
				assertTrue(file.getName(), imageView.isSelected());
				assertTrue(fileselectorButtonOk.isEnabled());
			}
		}

		solo.clickOnText(subDir, 2);
		instrumentation.waitForIdleSync();
		solo.clickOnText(filename);
		solo.clickOnText(activity.getString(android.R.string.ok));
		assertTrue("ok button clicked: ",
				solo.searchText("ok button clicked: "));
		solo.clickOnButton("finish");
		assertTrue("selected: ", solo.searchText("selected: "));
	}
}
