/**
 * 
 */
package de.uwegerdes.apps.fileselectorsampleapp.test;

import java.io.File;

import com.jayway.android.robotium.solo.Solo;

import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.FileSelectorAdapter;
import de.uwegerdes.apps.fileselectorsampleapp.MainActivity;
import de.uwegerdes.apps.fileselectorsampleapp.R;
import android.app.Instrumentation;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class TestSelectDirectory extends
		ActivityInstrumentationTestCase2<MainActivity> {
	public final static String TAG = "TestSelectDirectory";

	private MainActivity activity;
	private Instrumentation instrumentation;
	private Solo solo;

	private String rootDir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();

	@SuppressWarnings("deprecation")
	public TestSelectDirectory() {
		super("de.uwegerdes.apps.fileselectorsampleapp", MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		activity = getActivity();
		instrumentation = getInstrumentation();
		solo = new Solo(instrumentation, activity);
		instrumentation.waitForIdleSync();
	}

	@Override
	protected void tearDown() throws Exception {
		activity.finish();
		super.tearDown();
	}

	public void testStep10SelectDirectoryViewElements() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectDirectory)));
		solo.clickOnText(activity.getString(R.string.buttonSelectDirectory));
		instrumentation.waitForIdleSync();
		assertTrue(rootDir, solo.searchText(rootDir));

		// check directory view
		TextView directoryTextView = (TextView) solo
				.getCurrentActivity()
				.findViewById(
						de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(directoryTextView);
		String titleString = directoryTextView.getText().toString();
		assertEquals(rootDir, titleString);
		solo.goBack();
		instrumentation.waitForIdleSync();
		assertTrue("go back canceled", solo.searchText("canceled"));
		assertEquals(solo.getCurrentActivity().getClass(), MainActivity.class);
	}

	public void testStep20CheckChangeDirectory() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectDirectory)));
		solo.clickOnText(activity.getString(R.string.buttonSelectDirectory));
		instrumentation.waitForIdleSync();
		// assertTrue(solo.waitForFragmentByTag(FileSelectorFragment.TAG));
		// assertTrue(rootDir, solo.searchText(rootDir));
		// consumes a bit of time
		TextView directoryTextView = (TextView) solo
				.getCurrentActivity()
				.findViewById(
						de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(directoryTextView);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		assertNotNull(fileselectorListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		View listItemView = fileselectorListView.getChildAt(0);
		View radioView = listItemView
				.findViewById(R.id.fileselector_item_SelectRadio);
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());
		solo.clickOnView(radioView);
		// button should be enabled now
		assertTrue(fileselectorButtonOk.isEnabled());
		// select subdirectory
		// for (int i = 0; i < fileselectorListAdapter.getCount(); i++) {
		int maxIndex = (fileselectorListView.getChildCount() < 5) ? fileselectorListView
				.getChildCount() : 5;
		for (int i = 0; i < maxIndex; i++) {
			solo.scrollToTop();
			File file = fileselectorListAdapter.getItem(i);
			assertTrue(file.isDirectory());
			assertTrue(solo.searchText(file.getName()));
			solo.scrollToTop();
			if (file.isDirectory()) {
				View itemView = fileselectorListView.getChildAt(i);
				View nameView = itemView
						.findViewById(R.id.fileselector_item_Filename);
				solo.clickOnView(nameView);
				instrumentation.waitForIdleSync();
				solo.waitForFragmentByTag("wait a bit", 50);
				FileSelectorAdapter adapter = (FileSelectorAdapter) fileselectorListView
						.getAdapter();
				for (int j = 0; j < adapter.getCount(); j++) {
					assertEquals(adapter.getItem(j).getAbsolutePath(),
							file.getAbsolutePath(), adapter.getItem(j)
									.getParent());
				}
				solo.clickOnView(solo.getCurrentActivity().findViewById(
						R.id.fileselector_view_Directory));
				instrumentation.waitForIdleSync();
				// solo.waitForFragmentByTag("wait a bit", 50);
				assertFalse(fileselectorButtonOk.isEnabled());
			}
		}
		String titleString = directoryTextView.getText().toString();
		assertEquals(rootDir, titleString);
		assertFalse(fileselectorButtonOk.isEnabled());
		solo.clickOnButton(activity.getString(android.R.string.cancel));
		assertTrue("canceled", solo.searchText("canceled"));
		assertEquals(solo.getCurrentActivity().getClass(), MainActivity.class);
	}

	public void testStep30CheckDirectorySelection() {
		assertNotNull(activity);
		Log.v(TAG, solo.getCurrentActivity().getClass().toString());

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectDirectory)));
		solo.clickOnText(activity.getString(R.string.buttonSelectDirectory));
		instrumentation.waitForIdleSync();
		// assertTrue(solo.waitForFragmentByTag(FileSelectorFragment.TAG));
		// assertTrue(rootDir, solo.searchText(rootDir));
		// consumes a bit of time
		TextView directoryTextView = (TextView) solo
				.getCurrentActivity()
				.findViewById(
						de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(directoryTextView);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		assertNotNull(fileselectorListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());
		// select subdirectory
		int maxIndex = (fileselectorListView.getChildCount() < 5) ? fileselectorListView
				.getChildCount() : 5;
		for (int i = 0; i < maxIndex; i++) {
			solo.scrollToTop();
			File file = fileselectorListAdapter.getItem(i);
			assertTrue(file.isDirectory());
			assertTrue(solo.searchText(file.getName()));
			solo.scrollToTop();
			if (file.isDirectory()) {
				View itemView = fileselectorListView.getChildAt(i);
				View nameView = itemView
						.findViewById(R.id.fileselector_item_Filename);
				solo.clickOnView(nameView);
				instrumentation.waitForIdleSync();
				solo.waitForText("wait a bit", 1, 50l);
				solo.scrollToTop();
				FileSelectorAdapter adapter = (FileSelectorAdapter) fileselectorListView
						.getAdapter();
				for (int j = 0; j < adapter.getCount(); j++) {
					assertEquals(file.getAbsolutePath(), adapter.getItem(j)
							.getParent());
					View listItemView = fileselectorListView.getChildAt(j);
					View radioView = listItemView
							.findViewById(R.id.fileselector_item_SelectRadio);
					solo.clickOnView(radioView);
					assertTrue(fileselectorButtonOk.isEnabled());
					solo.clickOnView(radioView);
					assertFalse(fileselectorButtonOk.isEnabled());
				}
				solo.scrollToTop();
				solo.clickOnView(solo.getCurrentActivity().findViewById(
						R.id.fileselector_view_Directory));
				instrumentation.waitForIdleSync();
				assertFalse(fileselectorButtonOk.isEnabled());
				View listItemView = fileselectorListView.getChildAt(i);
				View radioView = listItemView
						.findViewById(R.id.fileselector_item_SelectRadio);
				solo.clickOnView(radioView);
				assertTrue(fileselectorButtonOk.isEnabled());
				solo.clickOnView(radioView);
				assertFalse(fileselectorButtonOk.isEnabled());
			}
		}
		// click on radio icon
		View listItemView = fileselectorListView.getChildAt(0);
		View radioView = listItemView
				.findViewById(R.id.fileselector_item_SelectRadio);
		solo.clickOnView(radioView);
		// button should be enabled now
		assertTrue(fileselectorButtonOk.isEnabled());
		solo.clickOnButton(activity.getString(android.R.string.ok));
		assertTrue("selected: ", solo.searchText("selected: "));
	}
}
