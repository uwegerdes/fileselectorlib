/**
 * 
 */
package de.uwegerdes.apps.fileselectorsampleapp.test;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import android.os.Environment;
import android.util.Log;
import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.io.DirectoryReader;
import junit.framework.TestCase;

/**
 * Test the directory reader and all it's options
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class TestDirectoryReader extends TestCase {
	public static final String TAG = "TestDirectoryReader";

	private String baseDir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private ArrayList<File> dirTree;

	protected void setUp() throws Exception {
		super.setUp();
		dirTree = getDirTree(new File(baseDir));
	}

	public void testStep10BaseReader() {
		DirectoryReader directoryReader = new DirectoryReader(
				FileSelector.SELECT_TYPE_FILE, "", "");
		ArrayList<File> files = directoryReader
				.readDirectory(new File(baseDir));
		// dirTreeToString();
		assertEquals(dirTree.size(), files.size());
		for (int i = 0; i < dirTree.size(); i++) {
			File testfile = dirTree.get(i);
			if (testfile.isDirectory()) {
				// TODO: Check subdirectory
				Log.e(TAG, "Check subdirectory missing");
				assertEquals(testfile.getAbsolutePath(),
						testfile.getAbsolutePath(), files.get(i)
								.getAbsolutePath());
			} else {
				assertEquals(testfile.getAbsolutePath(),
						testfile.getAbsolutePath(), files.get(i)
								.getAbsolutePath());
			}
		}
		//fail("no test for subdirectory");
	}

	public void testStep20TextfileReader() {
		DirectoryReader directoryReader = new DirectoryReader(
				FileSelector.SELECT_TYPE_FILE, "txt", "");
		ArrayList<File> files = directoryReader
				.readDirectory(new File(baseDir));
		int j = 0;
		for (int i = 0; i < dirTree.size(); i++) {
			File testfile = dirTree.get(i);
			if (!testfile.isDirectory()
					&& !(testfile.getName().endsWith("txt"))) {
				continue;
			}
			File readfile = files.get(j++);
			Log.e(TAG, "textfile: " + readfile.getAbsolutePath());
			assertEquals(testfile.getAbsolutePath(),
					testfile.getAbsolutePath(), readfile.getAbsolutePath());
		}
	}

	public void testStep30MimeTypeReader() {
		DirectoryReader directoryReader = new DirectoryReader(
				FileSelector.SELECT_TYPE_FILE, "", "text/plain");
		ArrayList<File> files = directoryReader
				.readDirectory(new File(baseDir));
		int j = 0;
		for (int i = 0; i < dirTree.size(); i++) {
			File testfile = dirTree.get(i);
			if (!testfile.isDirectory()
					&& !(testfile.getName().endsWith("txt"))) {
				continue;
			}
			File readfile = files.get(j++);
			Log.e(TAG, "mimefile: " + readfile.getAbsolutePath());
			assertEquals(testfile.getAbsolutePath(),
					testfile.getAbsolutePath(), readfile.getAbsolutePath());
		}
	}

	private ArrayList<File> getDirTree(File dir) {
		ArrayList<File> dirTree = new ArrayList<File>();
		for (File file : getDir(dir)) {
			dirTree.add(file);
		}
		return dirTree;
	}

	private ArrayList<File> getDir(File dir) {
		ArrayList<File> files = new ArrayList<File>();
		files.addAll(Arrays.asList(dir.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				// ignore hidden files
				return !pathname.getPath().contains("/.");
			}
		})));
		Collections.sort(files, new Comparator<File>() {
			public int compare(File file1, File file2) {
				return file1.getName().compareToIgnoreCase(file2.getName());
			}
		});
		return files;
	}
}
