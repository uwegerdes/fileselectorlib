/**
 * 
 */
package de.uwegerdes.apps.fileselectorsampleapp.test;

import com.jayway.android.robotium.solo.Solo;

import de.uwegerdes.apps.fileselectorsampleapp.MainActivity;
import de.uwegerdes.apps.fileselectorsampleapp.R;
import android.app.Instrumentation;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.ListView;

/**
 * testing state changes - orientation change should trigger onSaveInstanceState
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class TestFileSelectorSaveState extends
		ActivityInstrumentationTestCase2<MainActivity> {
	public final static String TAG = "TestFileSelectorSaveState";

	private MainActivity activity;
	private Instrumentation instrumentation;
	private Solo solo;

	private String rootDir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String subDir = "FileSelectorSampleApp";
	private String filename = "test_file.txt";

	@SuppressWarnings("deprecation")
	public TestFileSelectorSaveState() {
		super("de.uwegerdes.apps.fileselectorsampleapp", MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		activity = getActivity();
		instrumentation = getInstrumentation();
		solo = new Solo(instrumentation, activity);
		instrumentation.waitForIdleSync();
		subDir = activity.getString(R.string.app_name);
	}

	@Override
	protected void tearDown() throws Exception {
		solo.goBack();
		activity.finish();
		super.tearDown();
	}

	public void testStep10ActivityOrientationChange() {
		assertNotNull(activity);
		boolean isLandscape = false;
		View mainView = activity.findViewById(R.id.activity_main_container);
		if (mainView.getWidth() > mainView.getHeight()) {
			isLandscape = true;
		}
		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectFile)));
		solo.clickOnText(activity.getString(R.string.buttonSelectFile));
		solo.setActivityOrientation(Solo.PORTRAIT);

		instrumentation.waitForIdleSync();
		assertTrue(rootDir, solo.searchText(rootDir));
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		assertNotNull(fileselectorListView);
		solo.clickOnText(subDir, 2);
		assertTrue(solo.searchText(filename));
		solo.clickOnText(filename);
		solo.waitForFragmentByTag("wait a bit", 50);

		View radio = fileselectorListView.getChildAt(0).findViewById(
				R.id.fileselector_item_SelectRadio);
		assertTrue(radio.getVisibility() == View.VISIBLE);
		assertTrue(radio.isSelected());

		solo.setActivityOrientation(Solo.LANDSCAPE);

		assertTrue(solo.searchText("/" + subDir));

		assertTrue(solo.searchText(filename));
		radio = solo.getCurrentActivity().findViewById(
				R.id.fileselector_item_SelectRadio);
		assertTrue(radio.isSelected());
		solo.clickOnText(filename);
		solo.waitForText("wait a bit", 1, 50l);
		assertFalse(radio.isSelected());

		solo.setActivityOrientation(Solo.PORTRAIT);

		assertTrue(solo.searchText("/" + subDir));

		assertTrue(solo.searchText(filename));
		radio = solo.getCurrentActivity().findViewById(
				R.id.fileselector_item_SelectRadio);
		assertFalse(radio.isSelected());
		solo.clickOnText(filename);
		solo.waitForText("wait a bit", 1, 50l);
		assertTrue(radio.isSelected());
		solo.takeScreenshot("testStep10ActivityOrientationChange5");

		solo.clickOnText(activity.getString(android.R.string.ok));
		instrumentation.waitForIdleSync();
		solo.takeScreenshot("testStep10ActivityOrientationChange");
		assertTrue(solo.searchText("/"+subDir+"/"+filename));
		if (isLandscape) {
			solo.setActivityOrientation(Solo.LANDSCAPE);
		}
	}

	public void testStep20DialogOrientationChange() {
		assertNotNull(activity);
		boolean isLandscape = false;
		View mainView = activity.findViewById(R.id.activity_main_container);
		if (mainView.getWidth() > mainView.getHeight()) {
			isLandscape = true;
		}
		solo.setActivityOrientation(Solo.PORTRAIT);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectDialog)));
		solo.clickOnText(activity.getString(R.string.buttonSelectDialog));
		instrumentation.waitForIdleSync();
		assertTrue(solo.searchText(rootDir));
		assertTrue(solo.searchText(subDir));
		solo.clickOnText(subDir);
		assertTrue(solo.searchText(filename));
		solo.clickOnText(filename);
		solo.waitForText("wait a bit", 1, 500l);

		// check dialog
		View radio = solo.getView(R.id.fileselector_item_SelectRadio);
		assertNotNull(radio);
		assertTrue(radio.getVisibility() == View.VISIBLE);
		assertTrue(radio.isSelected());

		solo.setActivityOrientation(Solo.LANDSCAPE);

		assertTrue(solo.searchText("/" + subDir));

		assertTrue(solo.searchText(filename));
		radio = solo.getView(R.id.fileselector_item_SelectRadio);
		assertNotNull(radio);
		assertTrue(radio.isSelected());
		solo.clickOnText(filename);
		solo.waitForText("wait a bit", 1, 50l);
		assertFalse(radio.isSelected());

		solo.setActivityOrientation(Solo.PORTRAIT);

		assertTrue(solo.searchText("/" + subDir));

		assertTrue(solo.searchText(filename));
		radio = solo.getView(R.id.fileselector_item_SelectRadio);
		assertNotNull(radio);
		assertFalse(radio.isSelected());
		solo.clickOnText(filename);
		solo.waitForText("wait a bit", 1, 50l);
		assertTrue(radio.isSelected());

		solo.clickOnText(activity.getString(android.R.string.cancel));
		assertTrue(solo.searchText("main received canceled"));
		if (isLandscape) {
			solo.setActivityOrientation(Solo.LANDSCAPE);
		}
	}
}
