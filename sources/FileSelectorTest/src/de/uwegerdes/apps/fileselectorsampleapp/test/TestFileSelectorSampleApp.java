package de.uwegerdes.apps.fileselectorsampleapp.test;

import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.FileSelectorAdapter;
import de.uwegerdes.apps.fileselectorsampleapp.MainActivity;
import de.uwegerdes.apps.fileselectorsampleapp.R;

import com.jayway.android.robotium.solo.Solo;

import android.app.Activity;
import android.app.Instrumentation;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class TestFileSelectorSampleApp extends
		ActivityInstrumentationTestCase2<MainActivity> {
	public final static String TAG = "TestFileSelectorSampleApp";

	private MainActivity activity;
	private Instrumentation instrumentation;
	private Solo solo;

	private String rootDir = Environment.getExternalStorageDirectory()
			.getPath();

	@SuppressWarnings("deprecation")
	public TestFileSelectorSampleApp() {
		super("de.uwegerdes.apps.fileselectorsampleapp", MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		activity = getActivity();
		instrumentation = getInstrumentation();
		solo = new Solo(instrumentation, activity);
		instrumentation.waitForIdleSync();
	}

	@Override
	protected void tearDown() throws Exception {
		solo.goBack();
		activity.finish();
		super.tearDown();
	}

	public void testStep10SelectDirectory() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectDirectory)));
		solo.clickOnText(activity.getString(R.string.buttonSelectDirectory));
		instrumentation.waitForIdleSync();
		assertTrue(solo.searchText(rootDir));
		Activity myActivity = solo.getCurrentActivity();

		// check title view
		TextView title = (TextView) myActivity
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(title);
		String titleString = title.getText().toString();
		assertEquals(rootDir, titleString);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		assertTrue(fileselectorListView.getChildCount() <= fileselectorListAdapter
				.getCount());
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo
				.getView(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());

		solo.goBack();
	}

	public void testStep20SelectFile() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectFile)));
		solo.clickOnText(activity.getString(R.string.buttonSelectFile));
		instrumentation.waitForIdleSync();
		assertTrue(solo.searchText(rootDir));
		Activity myActivity = solo.getCurrentActivity();

		// check title view
		TextView title = (TextView) myActivity
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(title);
		String titleString = title.getText().toString();
		assertEquals(rootDir, titleString);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());

		solo.goBack();
	}

	public void testStep30Extension() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectFileExtension)));
		solo.clickOnText(activity.getString(R.string.buttonSelectFileExtension));
		instrumentation.waitForIdleSync();
		assertTrue(solo.searchText(rootDir));
		Activity myActivity = solo.getCurrentActivity();

		// check title view
		TextView title = (TextView) myActivity
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(title);
		String titleString = title.getText().toString();
		assertEquals(rootDir, titleString);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());

		solo.goBack();
	}

	public void testStep40MimeType() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectFileMimeType)));
		solo.clickOnText(activity.getString(R.string.buttonSelectFileMimeType));
		instrumentation.waitForIdleSync();
		assertTrue(solo.searchText(rootDir));
		Activity myActivity = solo.getCurrentActivity();

		// check title view
		TextView title = (TextView) myActivity
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(title);
		String titleString = title.getText().toString();
		assertEquals(rootDir, titleString);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());

		solo.goBack();
	}

	public void testStep50BaseDirectory() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectFileBaseDirectory)));
		solo.clickOnText(activity
				.getString(R.string.buttonSelectFileBaseDirectory));
		instrumentation.waitForIdleSync();
		assertTrue(solo.searchText(rootDir));
		Activity myActivity = solo.getCurrentActivity();

		// check title view
		TextView title = (TextView) myActivity
				.findViewById(de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(title);
		String titleString = title.getText().toString();
		assertEquals(rootDir + "/"+activity.getString(R.string.app_name), titleString);

		// check list content (only directories)
		ListView fileselectorListView = (ListView) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ListView);
		FileSelectorAdapter fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		assertTrue(fileselectorListAdapter.getCount() > 0);
		// check ok/cancel button
		Button fileselectorButtonOk = (Button) solo.getCurrentActivity()
				.findViewById(R.id.fileselector_view_ButtonOk);
		assertFalse(fileselectorButtonOk.isEnabled());

		solo.goBack();
	}

	public void testStep80SelectDialog() {
		assertNotNull(activity);

		assertTrue(solo.searchText(activity
				.getString(R.string.buttonSelectDialog)));
		solo.clickOnText(activity.getString(R.string.buttonSelectDialog));
		instrumentation.waitForIdleSync();
		assertTrue(solo.searchText(rootDir));

		// check title view
		TextView title = (TextView) solo
				.getView(de.uwegerdes.apps.fileselectorlib.R.id.fileselector_view_Directory);
		assertNotNull(title);
		String titleString = title.getText().toString();
		assertEquals(rootDir, titleString);
	}

}
