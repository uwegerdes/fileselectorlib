/**
 * 
 */
package de.uwegerdes.apps.fileselectorsampleapp.test;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorsampleapp.MainActivity;
import de.uwegerdes.apps.fileselectorsampleapp.R;
import android.content.res.Resources;
import android.test.ActivityInstrumentationTestCase2;

/**
 * 
 * @author Uwe Gerdes, Klaus-Groth-Str. 22, D-20535 Hamburg, apps@uwegerdes.de
 */
public class TestFileSelectorData extends
		ActivityInstrumentationTestCase2<MainActivity> {
	public final static String TAG = "TestFileSelectorData";

	/**
	 * @param activityClass
	 */
	@SuppressWarnings("deprecation")
	public TestFileSelectorData() {
		super("de.uwegerdes.apps.fileselectorsampleapp", MainActivity.class);
	}

	public void testStep10ResourceValues() {
		MainActivity activity = getActivity();
		assertNotNull(activity);

		Resources res = activity.getResources();
		String title = res.getResourceEntryName(R.attr.title);
		assertNotNull(title);
		assertEquals(title, FileSelector.TITLE);

		String selectType = res.getResourceEntryName(R.attr.selectType);
		assertNotNull(selectType);
		assertEquals(selectType, FileSelector.SELECT_TYPE);

		String extensions = res.getResourceEntryName(R.attr.extensions);
		assertNotNull(extensions);
		assertEquals(extensions, FileSelector.EXTENSIONS);

		String mimeTypes = res.getResourceEntryName(R.attr.mimeTypes);
		assertNotNull(mimeTypes);
		assertEquals(mimeTypes, FileSelector.MIMETYPES);

		String baseDirectory = res.getResourceEntryName(R.attr.baseDirectory);
		assertNotNull(baseDirectory);
		assertEquals(baseDirectory, FileSelector.BASE_DIRECTORY);

		String currentDirectory = res
				.getResourceEntryName(R.attr.currentDirectory);
		assertNotNull(currentDirectory);
		assertEquals(currentDirectory, FileSelector.CURRENT_DIRECTORY);

		String filename = res.getResourceEntryName(R.attr.filename);
		assertNotNull(filename);
		assertEquals(filename, FileSelector.FILENAME);

		String filetype = res.getResourceEntryName(R.attr.filetype);
		assertNotNull(filetype);
		assertEquals(filetype, FileSelector.FILETYPE);

		String selectedFile = res.getResourceEntryName(R.attr.selectedFile);
		assertNotNull(selectedFile);
		assertEquals(selectedFile, FileSelector.SELECTED_FILE);

		String onOkClick = res.getResourceEntryName(R.attr.onOkClick);
		assertNotNull(onOkClick);
		assertEquals(onOkClick, FileSelector.ON_OK_CLICK);

		String onCancelClick = res.getResourceEntryName(R.attr.onCancelClick);
		assertNotNull(onCancelClick);
		assertEquals(onCancelClick, FileSelector.ON_CANCEL_CLICK);

		String selectTypeDirectory = activity
				.getString(R.string.fileselector_selectType_directory);
		assertNotNull(selectTypeDirectory);
		assertEquals(selectTypeDirectory, FileSelector.SELECT_TYPE_DIRECTORY);

		String selectTypeFile = activity
				.getString(R.string.fileselector_selectType_file);
		assertNotNull(selectTypeFile);
		assertEquals(selectTypeFile, FileSelector.SELECT_TYPE_FILE);
	}

}
