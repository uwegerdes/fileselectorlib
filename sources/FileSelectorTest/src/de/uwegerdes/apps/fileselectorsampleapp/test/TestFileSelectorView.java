package de.uwegerdes.apps.fileselectorsampleapp.test;

import java.io.File;
import java.util.Calendar;

import de.uwegerdes.apps.fileselectorlib.FileSelector;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView.FileSelectorAdapter;
import de.uwegerdes.apps.fileselectorlib.widget.FileSelectorView;
import de.uwegerdes.apps.fileselectorsampleapp.R;

import android.os.Environment;
import android.test.AndroidTestCase;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class TestFileSelectorView extends AndroidTestCase {
	public final static String TAG = "TestFileSelectorView";

	private FileSelectorView fileSelectorView;
	private TextView fileselectorDirectory;
	private ListView fileselectorListView;
	private FileSelectorAdapter fileselectorListAdapter;
	private ImageButton fileselectorButtonUp;
	private Button fileselectorButtonOk;
	private Button fileselectorButtonCancel;

	private String rootDir = Environment.getExternalStorageDirectory()
			.getAbsolutePath();

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testStep10CreateDefaultView() {
		fileSelectorView = new FileSelectorView(getContext());
		assertNotNull(fileSelectorView);
		fileSelectorView.setUpForTest();
		findViewElements(fileSelectorView);
		assertNotNull(fileSelectorView.findViewById(R.id.fileselector_view_Directory));

		// check the view elements
		assertNotNull(fileselectorDirectory);
		assertNotNull(fileselectorListView);
		assertNotNull(fileselectorListAdapter);
		assertNotNull(fileselectorButtonUp);
		assertNotNull(fileselectorButtonOk);
		assertNotNull(fileselectorButtonCancel);
		// check contents
		//assertEquals(rootDir, fileselectorDirectory.getText().toString()); //$NON-NLS-1$
		assertFalse(fileselectorButtonUp.getVisibility() == View.GONE);
		assertTrue(fileselectorButtonOk.getVisibility() == View.VISIBLE);
		assertFalse(fileselectorButtonOk.isEnabled());
		assertEquals(getString(android.R.string.ok), fileselectorButtonOk
				.getText().toString());
		assertTrue(fileselectorButtonCancel.getVisibility() == View.VISIBLE);
		assertEquals(getString(android.R.string.cancel),
				fileselectorButtonCancel.getText().toString());
	}

	public void testStep11CreateDirectorySelector() {
		fileSelectorView = new FileSelectorView(getContext(),
				FileSelector.SELECT_TYPE_DIRECTORY, "", "", rootDir);
		assertNotNull(fileSelectorView);
		fileSelectorView.setUpForTest();
		assertNotNull(fileSelectorView.findViewById(R.id.fileselector_view_Directory));
		findViewElements(fileSelectorView);

		// check the view elements
		assertNotNull(fileselectorDirectory);
		assertNotNull(fileselectorListView);
		assertNotNull(fileselectorListAdapter);
		assertNotNull(fileselectorButtonUp);
		assertNotNull(fileselectorButtonOk);
		assertNotNull(fileselectorButtonCancel);
		// check contents
		//assertEquals(rootDir, fileselectorDirectory.getText().toString()); //$NON-NLS-1$
		assertFalse(fileselectorButtonUp.getVisibility() == View.GONE);
		assertTrue(fileselectorButtonOk.getVisibility() == View.VISIBLE);
		assertFalse(fileselectorButtonOk.isEnabled());
		assertEquals(getString(android.R.string.ok), fileselectorButtonOk
				.getText().toString());
		assertTrue(fileselectorButtonCancel.getVisibility() == View.VISIBLE);
		assertEquals(getString(android.R.string.cancel),
				fileselectorButtonCancel.getText().toString());
	}

	public void testStep20FileSelectorAdapter() {
		fileSelectorView = new FileSelectorView(getContext());
		assertNotNull(fileSelectorView);
		fileSelectorView.setUpForTest();
		assertNotNull(fileSelectorView.findViewById(R.id.fileselector_view_Directory));
		findViewElements(fileSelectorView);

		// check adapter
		assertTrue(fileselectorListAdapter.getCount() > 0);
		ViewGroup childView = null;
		TextView fileselectorFilename;
		TextView fileselectorFileInfo;
		ImageView fileselectorFileIcon;
		View fileSelectorRadio;

		for (int i = 0; i < fileselectorListAdapter.getCount(); i++) {
			File file = fileselectorListAdapter.getItem(i);
			assertFalse(file.getName().startsWith("."));
			assertTrue(file.exists());
			assertTrue(file.canRead());
			childView = (ViewGroup) fileselectorListAdapter.getView(i,
					childView, null);
			assertNotNull(childView);
			fileselectorFilename = (TextView) childView
					.findViewById(R.id.fileselector_item_Filename);
			fileselectorFileInfo = (TextView) childView
					.findViewById(R.id.fileselector_item_FileInfo);
			fileselectorFileIcon = (ImageView) childView
					.findViewById(R.id.fileselector_item_FileIcon);
			fileSelectorRadio = childView
					.findViewById(R.id.fileselector_item_SelectRadio);
			assertNotNull(fileselectorFilename);
			assertNotNull(fileselectorFileInfo);
			assertNotNull(fileselectorFileIcon);
			if (file.isDirectory()) {
				assertTrue(fileselectorFileInfo.getVisibility() == View.GONE);
				assertTrue(fileselectorFileIcon.getVisibility() == View.VISIBLE);
				assertTrue(fileselectorFileIcon.getDrawable() != null);
				assertTrue(fileSelectorRadio.getVisibility() == View.GONE);
			} else if (file.isFile()) {
				assertTrue(fileselectorFileInfo.getVisibility() == View.VISIBLE);
				assertTrue(fileselectorFileInfo.getText().length() > 0);
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(file.lastModified());
				assertEquals(FileSelectorView.formatDate(calendar) + ", "
						+ FileSelectorView.formatSize(file.length()),
						fileselectorFileInfo.getText().toString());
				assertFalse(fileselectorFileIcon.getVisibility() == View.VISIBLE);
				assertTrue(fileselectorFileIcon.getDrawable() == null);
				assertTrue(fileSelectorRadio.getVisibility() == View.VISIBLE);
			}
		}
	}

	public void testStep21DirectorySelectorAdapter() {
		fileSelectorView = new FileSelectorView(getContext(),
				FileSelector.SELECT_TYPE_DIRECTORY, "", "", rootDir);
		assertNotNull(fileSelectorView);
		fileSelectorView.setUpForTest();
		assertNotNull(fileSelectorView.findViewById(R.id.fileselector_view_Directory));
		findViewElements(fileSelectorView);

		// check adapter
		assertTrue(fileselectorListAdapter.getCount() > 0);
		ViewGroup childView = null;
		TextView fileselectorFilename;
		TextView fileselectorFileInfo;
		ImageView fileselectorFileIcon;
		View fileSelectorRadio;

		for (int i = 0; i < fileselectorListAdapter.getCount(); i++) {
			File file = fileselectorListAdapter.getItem(i);
			assertFalse(file.getName().startsWith("."));
			assertTrue(file.getName(), file.exists());
			assertTrue(file.getName(), file.canRead());
			assertTrue(file.getName(), file.isDirectory());
			childView = (ViewGroup) fileselectorListAdapter.getView(i,
					childView, null);
			assertNotNull(file.getName(), childView);
			fileselectorFilename = (TextView) childView
					.findViewById(R.id.fileselector_item_Filename);
			fileselectorFileInfo = (TextView) childView
					.findViewById(R.id.fileselector_item_FileInfo);
			fileselectorFileIcon = (ImageView) childView
					.findViewById(R.id.fileselector_item_FileIcon);
			fileSelectorRadio = childView
					.findViewById(R.id.fileselector_item_SelectRadio);
			assertNotNull(file.getName(), fileselectorFilename);
			assertNotNull(file.getName(), fileselectorFileInfo);
			assertNotNull(file.getName(), fileselectorFileIcon);
			assertTrue(file.getName(),
					fileselectorFileInfo.getVisibility() == View.GONE);
			assertTrue(file.getName(),
					fileselectorFileIcon.getVisibility() == View.VISIBLE);
			assertTrue(file.getName(),
					fileselectorFileIcon.getDrawable() != null);
			assertTrue(file.getName(),
					fileSelectorRadio.getVisibility() == View.VISIBLE);
		}
	}

	public void testStep30ChangeDirectory() {
		fileSelectorView = new FileSelectorView(getContext());
		assertNotNull(fileSelectorView);
		fileSelectorView.setUpForTest();
		assertNotNull(fileSelectorView.findViewById(R.id.fileselector_view_Directory));
		findViewElements(fileSelectorView);

		View childView = null;
		// for every directory
		for (int i = 0; i < fileselectorListAdapter.getCount(); i++) {
			File file = fileselectorListAdapter.getItem(i);
			childView = fileselectorListAdapter.getView(i, childView, null);
			if (file.isDirectory()) {
				// open directory
				fileselectorListAdapter.setDirectory(file.getAbsoluteFile());
				// check result
				assertEquals(file.getAbsolutePath(), fileselectorDirectory
						.getText().toString());
				for (int j = 0; j < fileselectorListAdapter.getCount(); j++) {
					assertEquals(file.getAbsolutePath(),
							fileselectorListAdapter.getItem(j).getParent());
				}
				// go back
				fileSelectorView.goUp();
			}
			assertEquals(file.getParent(), fileselectorDirectory.getText()
					.toString());
		}
	}

	// public void testStep50() {
	// fail("no tests implemented");
	// }

	/**
	 * get the view elements for the test
	 * 
	 * @param fileSelectorView2
	 */

	private void findViewElements(FileSelectorView fileSelectorView) {
		fileselectorDirectory = (TextView) fileSelectorView
				.findViewById(R.id.fileselector_view_Directory);
		fileselectorListView = (ListView) fileSelectorView
				.findViewById(R.id.fileselector_view_ListView);
		fileselectorListAdapter = (FileSelectorAdapter) fileselectorListView
				.getAdapter();
		fileselectorButtonUp = (ImageButton) fileSelectorView
				.findViewById(R.id.fileselector_view_ButtonUp);
		fileselectorButtonOk = (Button) fileSelectorView
				.findViewById(R.id.fileselector_view_ButtonOk);
		fileselectorButtonCancel = (Button) fileSelectorView
				.findViewById(R.id.fileselector_view_ButtonCancel);
	}

	/**
	 * get a string from context
	 * 
	 * @param resId
	 * @return string from resoures
	 */
	private String getString(int resId) {
		return getContext().getString(resId);
	}

}
